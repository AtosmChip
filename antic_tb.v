// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "clock.v"
`include "atari.v"

module antic_tb();
   wire clk;
   wire clk2;

   reg rst_o;

   initial begin
      $readmemh("os/atarios.memh", u_atari.u_rom.memory);
      $readmemh("tests/antic_test.memh", u_atari.u_rom.memory, 'h3000);

      $monitor("%h %h l%b s%b %b %b v=%h adro=%h ir=%h d=%h dlptr=%h memsc=%h max=%h out=%b",
 	       u_atari.u_antic.shift_reg_out,
 	       u_atari.u_antic.u_shift_reg.in,
 	       u_atari.u_antic.u_shift_reg.load,
 	       u_atari.u_antic.u_shift_reg.shift,
 	       u_atari.u_antic.load_char,
 	       u_atari.u_antic.char_block,
 	       u_atari.u_antic.vcount,
 	       u_atari.u_antic.adr_o,
 	       u_atari.u_antic.ir,
 	       u_atari.u_antic.dcount,
 	       u_atari.u_antic.dlist_ctr,
 	       u_atari.u_antic.memscan_ctr,
 	       u_atari.u_antic.maxline,
 	       u_atari.u_antic.out_reg);

      //$monitor("%b %b %b %b",
	//       u_atari.u_antic.dma_instr_en,
	//       u_atari.u_antic.new_block,
	//       u_atari.u_antic.vblank,
	//       u_atari.u_antic.wait_vblank);

      rst_o = 0;
      #5 rst_o = 1;
      #40 rst_o = 0;
      #200_000_0;
      $finish;
   end

   always @ (posedge clk) begin
      if (u_atari.adr >= 'hd000 && u_atari.adr < 'hd800) begin
	 if (u_atari.we)
	   $display("hardware write %h = %h at %h",
		    u_atari.adr, u_atari.masterdat_o, u_atari.u_cpu.pc);
	 else
	   $display("hardware read  %h = %h at %h",
		    u_atari.adr, u_atari.slavedat_o, u_atari.u_cpu.pc);
      end
   end

   always @ (posedge u_atari.vsync)
     $finishframe;

   always @ (posedge u_atari.hsync)
     $finishline;

   always @ (posedge clk2 or negedge clk2)
     $outpixel(u_atari.color);

   clkgen u_clkgen(.clk_o(clk), .clk2_o(clk2));
   atari u_atari(.clk_i(clk), .clk2_i(clk2), .rst_i(rst_o));
endmodule
