// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

module ram(ack_o,
	   clk_i,
	   adr_i,
	   dat_i,
	   dat_o,
	   rst_i,
	   stb_i,
	   we_i);
   parameter size = 1024;
   parameter adrbits = 10;

   input clk_i;
   input adr_i;
   input dat_i;
   input rst_i;
   input stb_i;
   input we_i;
   output ack_o;
   output dat_o;

   wire       clk_i;
   wire [adrbits - 1:0] adr_i;
   wire [7:0] dat_i;
   wire       rst_i;
   wire       stb_i;
   wire       we_i;

   wire       ack_o;
   wire [7:0] dat_o;

   reg [7:0]  memory [0:size - 1];

   assign     ack_o = stb_i;
   assign     dat_o = memory[adr_i];
 
   always @ (posedge clk_i)
     if (stb_i == 1'b1 && we_i == 1'b1)
       memory[adr_i] = dat_i;
		       
endmodule

module rom(ack_o,
	   clk_i,
	   adr_i,
	   dat_o,
	   rst_i,
	   stb_i);
   parameter size = 1024;
   parameter adrbits = 10;

   input clk_i;
   input adr_i;
   input rst_i;
   input stb_i;
   output ack_o;
   output dat_o;

   wire       clk_i;
   wire [adrbits - 1:0] adr_i;
   wire       rst_i;
   wire       stb_i;

   wire       ack_o;
   wire [7:0] dat_o;

   reg [7:0]  memory [0:size - 1];

   assign     ack_o = stb_i;
   assign     dat_o = memory[adr_i];
 
endmodule
