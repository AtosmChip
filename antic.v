// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

module antic_ms_hcount_seq(clk_i, ms_hcount,
			   new_block, dma_block, char_block, dma_pf_width, ir,
			   shift_reg_shift, load_pf, load_char, load_out,
			   out_reg_shift);
   input clk_i, ms_hcount;
   input new_block, dma_block, char_block, dma_pf_width, ir;
   output shift_reg_shift, load_pf, load_char, load_out, out_reg_shift;

   wire       clk_i;
   wire [7:0] ms_hcount;
   wire       new_block, dma_block, char_block;
   wire [1:0] dma_pf_width;
   wire [7:0] ir;
   wire       shift_reg_shift;
   reg 	      load_pf, load_char;
   wire       load_out;
   wire       out_reg_shift;

   reg 	      load_pf_0, load_char_0;
   reg [3:0]  pf_byte_mod;
   reg [1:0]  pf_pixel_mod;

   integer    pf_cyc;

   always @ (ir)
     case (ir[3:0])
       'h2: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'h3: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'h4: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'h5: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'h6: begin
	  pf_byte_mod = 7;
	  pf_pixel_mod = 0;
       end
       'h7: begin
	  pf_byte_mod = 7;
	  pf_pixel_mod = 0;
       end
       'h8: begin
	  pf_byte_mod = 15;
	  pf_pixel_mod = 3;
       end
       'h9: begin
	  pf_byte_mod = 15;
	  pf_pixel_mod = 1;
       end
       'ha: begin
	  pf_byte_mod = 7;
	  pf_pixel_mod = 1;
       end
       'hb: begin
	  pf_byte_mod = 7;
	  pf_pixel_mod = 0;
       end
       'hc: begin
	  pf_byte_mod = 7;
	  pf_pixel_mod = 0;
       end
       'hd: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'he: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       'hf: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
       default: begin
	  pf_byte_mod = 3;
	  pf_pixel_mod = 0;
       end
     endcase

   assign     shift_reg_shift = (ms_hcount >= 3) && (ms_hcount < 192 + 3) &&
				(ms_hcount[1:0] == 3);

   assign     out_reg_shift = ((ms_hcount[1:0] & pf_pixel_mod) ==
			       (2'd2 & pf_pixel_mod));

   always @ (new_block or dma_block or dma_pf_width or ms_hcount or
	     pf_byte_mod or ir) begin
      pf_cyc = char_block ? 3 : 7;
      load_pf_0 = 0;
      if (new_block && dma_block) begin
	 if (dma_pf_width == 1 && !ir[4])
	   load_pf_0 = ((ms_hcount & pf_byte_mod) == (pf_cyc & pf_byte_mod)) &&
		       (ms_hcount >= 32 + pf_cyc) &&
		       (ms_hcount < 160 + pf_cyc);
	 else if (dma_pf_width == (ir[4] ? 1 : 2))
      	   load_pf_0 = ((ms_hcount & pf_byte_mod) == (pf_cyc & pf_byte_mod)) &&
		       (ms_hcount >= 16 + pf_cyc) &&
		       (ms_hcount < 176 + pf_cyc);
	 else if (dma_pf_width == 3 || (ir[4] && dma_pf_width == 2))
	   load_pf_0 = ((ms_hcount & pf_byte_mod) == (pf_cyc & pf_byte_mod)) &&
		       (ms_hcount >= pf_cyc) && (ms_hcount < 192 + pf_cyc);
      end
   end

   always @ (char_block or dma_block or dma_pf_width or ms_hcount or
	     pf_byte_mod) begin
      load_char_0 = 0;
      if (char_block && dma_block) begin
	 if (dma_pf_width == 1 && !ir[4])
	   load_char_0 = ((ms_hcount & pf_byte_mod) == (9 & pf_byte_mod)) &&
			 (ms_hcount >= 32 + 9 && ms_hcount < 160 + 9);
	 else if (dma_pf_width == (ir[4] ? 1 : 2))
	   load_char_0 = ((ms_hcount & pf_byte_mod) == (9 & pf_byte_mod)) &&
			 (ms_hcount >= 16 + 9 && ms_hcount < 176 + 9);
	 else if (dma_pf_width == 3 || (ir[4] && dma_pf_width == 2))
	   load_char_0 = ((ms_hcount & pf_byte_mod) == (9 & pf_byte_mod)) &&
			 (ms_hcount >= 9 && ms_hcount < 192 + 9);
      end
   end

   assign load_out = ((ms_hcount[3:0] & pf_byte_mod) == (4'd14 & pf_byte_mod));

   always @ (posedge clk_i) begin
      load_pf <= load_pf_0;
      load_char <= load_char_0;
   end
endmodule

module antic_shift_reg(clk_i, shift, load, in, out);
   input clk_i;
   input shift;
   input load;
   input in;

   output out;

   wire  clk_i;
   wire  shift;
   wire  load;
   wire [7:0] in;
   wire [7:0] out;

   reg [7:0] shift_reg [0:47];
   
   integer   i;

   assign    out = shift_reg[1];

   always @ (posedge clk_i) begin
      if (shift)
	for (i = 0; i < 47; i = i + 1)
	  shift_reg[i + 1] <= shift_reg[i];
      if (load)
	shift_reg[0] <= in;
      else if (shift)
       shift_reg[0] <= shift_reg[47];
   end
endmodule

module antic(rst_i, clk_i,
	     adr_i, adr_o,
	     slavedat_i, masterdat_i,
	     dat_o,
	     we_i,
	     stb_i, stb_o,
	     ack_i, ack_o,
	     cyc_o,
	     clk2_i,
	     nmi,
	     antic_out);
   input rst_i;
   input clk_i;
   input adr_i;
   input slavedat_i;
   input masterdat_i;
   input we_i;
   input stb_i;
   input ack_i;
   input clk2_i;

   output adr_o;
   output dat_o;
   output stb_o;
   output ack_o;
   output cyc_o;
   output nmi;
   output antic_out;

   wire       rst_i, clk_i;
   wire [3:0] adr_i;
   wire [7:0] slavedat_i;
   wire [7:0] masterdat_i;
   wire       we_i;
   wire       stb_i;
   wire       ack_i;
   wire       clk2_i;
       
   reg [15:0] adr_o;
   reg [7:0]  dat_o;
   wire	      stb_o;
   wire       ack_o;
   wire       cyc_o;
   wire       nmi;
   reg [2:0]  pf_antic_out, pf_antic_out_d;
   reg [2:0]  antic_out;

   reg [1:0]  dma_pf_width;
   reg 	      dma_mis_en;
   reg 	      dma_ply_en;
   reg 	      dma_pm_1res;
   reg 	      dma_instr_en;

   reg [6:0]  chbase;
   reg [2:0]  chactl;
   reg [5:0]  pmbase;
   reg [3:0]  hscrol;
   reg [3:0]  vscrol;

   wire       nmireq_dli, nmireq_vbi;
   reg 	      nmist_dli, nmist_vbi;
   reg 	      nmien_dli, nmien_vbi;

   reg [15:0] dlist_ctr;
   reg [7:0]  dlist_ctr_tmp;
   wire	      dl_load;

   reg [15:0] memscan_ctr;

   reg [7:0]  hcount;
   reg [8:0]  vcount;
   reg [3:0]  dcount;
   wire [3:0] chr_dcount;
   reg [7:0]  ms_hcount;

   reg [7:0]  ir;
   reg 	      new_block;
   reg [3:0]  maxline;
   wire       dli;
   wire       wait_vblank;
   wire       dma_block;
   reg 	      char_block;
   reg 	      one_bit_pixel;
   wire       vscrol_en;
   reg 	      last_vscrol_en;

   reg 	      wsync;

   wire       load_instr;
   wire       load_dlptrl;
   wire       load_dlptrh;
   wire       load_memscanl;
   wire       load_memscanh;
   wire       load_mis;
   wire       load_ply;
   wire       load_pf;
   wire       load_char;

   reg 	      refresh_req;
   reg 	      refresh_ack;

   wire [1:0] dma_ply_num;

   wire       load_out_p, load_out;

   wire       hblank, vblank, vsync;
   reg 	      dwin;
      
   wire	      shift_reg_shift;
   wire [7:0] shift_reg_out;
   wire       char_blank;
   reg [7:0]  char_data, out_reg;
   wire       out_reg_shift;
   reg [1:0]  char_color_p, char_color;

   assign ack_o = stb_i;

   // Read registers.
   always @ (adr_i or vcount or nmist_dli or nmist_vbi)
     case (adr_i)
       'hb:
	 dat_o = vcount[8:1];
       'hf:
	 dat_o = {nmist_dli, nmist_vbi, 6'b0};
       default:
	 dat_o = 'hff;
     endcase

   // DMACTL
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h0) begin
	dma_pf_width <= slavedat_i[1:0];
	dma_mis_en   <= slavedat_i[2];
	dma_ply_en   <= slavedat_i[3];
	dma_pm_1res  <= slavedat_i[4];
	dma_instr_en <= slavedat_i[5];
     end
	
   // CHACTL
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h1)
	chactl <= slavedat_i[2:0];

   // DLISTL/H
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h2)
       dlist_ctr[7:0] <= slavedat_i;
     else if (stb_i && we_i && adr_i == 'h3)
       dlist_ctr[15:8] <= slavedat_i;
     else if (dl_load) begin
	if (!load_dlptrh)
	  dlist_ctr[9:0] <= dlist_ctr[9:0] + 1;
	else begin
	   dlist_ctr[15:8] <= masterdat_i;
	   dlist_ctr[7:0] <= dlist_ctr_tmp;
	end
	if (load_dlptrl)
	  dlist_ctr_tmp <= masterdat_i;
     end

   // HSCROL
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h4)
       hscrol <= slavedat_i[3:0];

   // VSCROL
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h5)
       vscrol <= slavedat_i[3:0];

   // PMBASE
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h7)
       pmbase <= slavedat_i[7:2];

   // CHBASE
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h9)
       chbase <= slavedat_i[7:1];

   // WSYNC
   always @ (posedge clk_i)
     if (rst_i || hcount == 206)
       wsync <= 0;
     else if (stb_i && we_i && adr_i == 'ha)
       wsync <= 1;

   // NMIEN
   always @ (posedge clk_i)
     if (rst_i) begin
	nmien_vbi <= 0;
	nmien_dli <= 0;
     end else if (stb_i && we_i && adr_i == 'he) begin
	nmien_vbi <= slavedat_i[6];
	nmien_dli <= slavedat_i[7];
     end

   // HCOUNT
   always @ (posedge clk2_i)
     if (rst_i && !clk_i)
       hcount <= 0;
     else if (hcount == 227)
       hcount <= 0;
     else
       hcount <= hcount + 1;

   // VCOUNT
   always @ (posedge clk2_i)
     if (rst_i && !clk_i)
       vcount <= 0;
     else if (hcount == 227)
       if (vcount == 311)
	 vcount <= 0;
       else
	 vcount <= vcount + 1;

   // Display list interrupt.
   assign nmireq_dli = (hcount == 16 && dcount == maxline && dli &&
			nmien_dli && !vblank && !wait_vblank);

   // Vertical blank interrupt.
   assign nmireq_vbi = (hcount == 16 && vcount == 240 && nmien_vbi);

   always @ (posedge clk_i)
     if (rst_i) begin
	nmist_vbi <= 0;
	nmist_dli <= 0;
     end else if (nmireq_vbi) begin
	nmist_vbi <= 1;
	nmist_dli <= 0;
     end else if (nmireq_dli) begin
	nmist_vbi <= 0;
	nmist_dli <= 1;
     end else if (stb_i && we_i && adr_i =='hf) begin
       nmist_vbi <= 0;
       nmist_dli <= 0;
     end

   assign nmi = nmireq_dli | nmireq_vbi;

   always @ (posedge clk2_i)
     if (hcount == 227)
       if (dma_instr_en &&
	   (vcount == 7 ||
	    (dcount == ((last_vscrol_en && !vscrol_en) ? vscrol : maxline) &&
	     !wait_vblank && !vblank && dma_instr_en)))
	 new_block <= 1;
       else
	 new_block <= 0;

   assign load_instr = new_block && (hcount == 2);

   assign vscrol_en = dma_block && ir[5];

   always @ (posedge clk2_i)
     if (hcount == 0 && new_block)
       last_vscrol_en <= vscrol_en;

   // DCOUNT
   always @ (posedge clk2_i)
     if (vcount == 0)
       dcount <= 0;
     else if (hcount == 6)
       if (new_block)
	 dcount <= (!last_vscrol_en && vscrol_en) ? vscrol : 0;
       else
	 dcount <= dcount + 1;

   // Memory Scan Counter.
   always @ (posedge clk_i)
     if (load_pf)
       memscan_ctr[11:0] <= memscan_ctr[11:0] + 1;
     else if (load_memscanl)
       memscan_ctr[7:0] <= masterdat_i;
     else if (load_memscanh)
       memscan_ctr[15:8] <= masterdat_i;

   // Instruction register.
   always @ (posedge clk_i)
     if (load_instr)
       ir <= masterdat_i;
     else if (vcount == 0)
       ir <= 0;

   // Instruction decoder.
   always @ (ir) begin
      maxline = 0;
      char_block = 0;
      one_bit_pixel = 0;
      case (ir[3:0])
	'h0: maxline = ir[6:4];
	'h1: maxline = 0;
	'h2: begin
	   maxline = 7;
	   char_block = 1;
	   one_bit_pixel = 0;
	end
	'h3: begin
	   maxline = 9;
	   char_block = 1;
	   one_bit_pixel = 0;
	end
	'h4: begin
	   maxline = 7;
	   char_block = 1;
	   one_bit_pixel = 0;
	end
	'h5: begin
	   maxline = 15;
	   char_block = 1;
	   one_bit_pixel = 0;
	end
	'h6: begin
	   maxline = 7;
	   char_block = 1;
	   one_bit_pixel = 1;
	end
	'h7: begin
	   maxline = 15;
	   char_block = 1;
	   one_bit_pixel = 1;
	end
	'h8: begin
	   maxline = 7;
	   char_block = 0;
	   one_bit_pixel = 0;
	end
	'h9: begin
	   maxline = 3;
	   char_block = 0;
	   one_bit_pixel = 1;
	end
	'ha: begin
	   maxline = 3;
	   char_block = 0;
	   one_bit_pixel = 0;
	end
	'hb: begin
	   maxline = 1;
	   char_block = 0;
	   one_bit_pixel = 1;
	end
	'hc: begin
	   maxline = 0;
	   char_block = 0;
	   one_bit_pixel = 1;
	end
	'hd: begin
	   maxline = 1;
	   char_block = 0;
	   one_bit_pixel = 0;
	end
	'he: begin
	   maxline = 0;
	   char_block = 0;
	   one_bit_pixel = 0;
	end
	'hf: begin
	   maxline = 0;
	   char_block = 0;
	   one_bit_pixel = 0;
	end
      endcase
   end

   assign dli = ir[7];
   assign wait_vblank = (ir == 'h41);
   assign dma_block = (ir[3:0] != 0 && ir[3:0] != 1);

   assign load_dlptrl = new_block && (ir[3:0] == 1) && (hcount == 12);
   assign load_dlptrh = new_block && (ir[3:0] == 1) && (hcount == 14);
   
   assign load_memscanl = new_block && dma_block && ir[6] && (hcount == 12);
   assign load_memscanh = new_block && dma_block && ir[6] && (hcount == 14);
			 
   assign load_mis = !vblank && dma_mis_en && (hcount == 0);
   assign load_ply = !vblank && dma_ply_en &&
		     (hcount == 4 || hcount == 6 ||
		      hcount == 8 || hcount == 10);
   assign dma_ply_num = (hcount >> 1) - 2;

   assign dl_load = load_instr || load_memscanh || load_memscanl ||
		    load_dlptrh || load_dlptrl;

   always @ (posedge clk2_i)
     if (hcount == 16 + (ir[4] ? (hscrol & ~1) : 0))
       ms_hcount <= 0;
     else
       ms_hcount <= ms_hcount + 1;

   antic_ms_hcount_seq u_ms_hcount_seq(.clk_i(clk_i),
				       .ms_hcount(ms_hcount),
				       .new_block(new_block),
				       .dma_block(dma_block),
				       .char_block(char_block),
				       .dma_pf_width(dma_pf_width),
				       .ir(ir),
				       .shift_reg_shift(shift_reg_shift),
				       .load_pf(load_pf),
				       .load_char(load_char),
				       .load_out(load_out),
				       .out_reg_shift(out_reg_shift));

   always @ (hcount or dma_pf_width or dma_instr_en or vblank) begin
      if (!dma_instr_en || vblank)
	dwin = 0;
      else
	case (dma_pf_width)
	  0: dwin = 0;
	  1: dwin = (hcount >= 64 && hcount < 192);
	  2: dwin = (hcount >= 48 && hcount < 208);
	  3: dwin = (hcount >= 44 && hcount < 220);
	endcase
   end

   assign hblank = (hcount < 34 || hcount >= 222);
   assign vblank = (vcount < 8 || vcount >= 240);

   // TODO: lines here are approximate.
   assign vsync  = (vcount >= 300 && vcount < 303);

   assign char_blank = (ir[3:0] == 3) &&
		       (((chr_dcount == 0 || chr_dcount == 1) &&
			 shift_reg_out[6:5] == 2'b11) ||
			((chr_dcount == 8 || chr_dcount == 9) &&
			 shift_reg_out[6:5] != 2'b11));

   always @ (posedge clk_i)
     if (load_char) begin
	char_data <= char_blank ? 0 : masterdat_i;
	char_color_p <= shift_reg_out[7:6];
     end

   always @ (posedge clk2_i)
     if (load_out) begin
	out_reg <= char_block ? char_data : shift_reg_out;
	char_color <= char_color_p;
     end
     else if (out_reg_shift)
       if (one_bit_pixel)
	 out_reg <= {out_reg[6:0], 1'b0};
       else
	 out_reg <= {out_reg[5:0], 2'b00};

   always @ (ir or out_reg or char_color or chactl)
     if (ir[3:0] == 2 || ir[3:0] == 3)
       if (char_color[1])
	 pf_antic_out = {1'b1,
			 (out_reg[7:6] & ~{2{chactl[0]}}) ^ {2{chactl[1]}}};
       else
	 pf_antic_out = {1'b1, out_reg[7:6]};
     else if (ir[3:0] == 'hf)
       pf_antic_out = {1'b1, out_reg[7:6]};
     else if (ir[3:0] == 4 || ir[3:0] == 5)
       case (out_reg[7:6])
	 0: pf_antic_out = 3'b000;
	 1: pf_antic_out = 3'b100;
	 2: pf_antic_out = 3'b101;
	 3: pf_antic_out = char_color[1] ? 3'b111 : 3'b110;
       endcase
     else if (ir[3:0] == 6 || ir[3:0] == 7)
       if (out_reg[7])
	 pf_antic_out = {1'b1, char_color};
       else
	 pf_antic_out = 3'b000;
     else if (ir[3:0] == 8 || ir[3:0] == 'ha || ir[3:0] == 'hd ||
	      ir[3:0] == 'he)
       case (out_reg[7:6])
	 0: pf_antic_out = 3'b000;
	 1: pf_antic_out = 3'b100;
	 2: pf_antic_out = 3'b101;
	 3: pf_antic_out = 3'b110;
       endcase
     else if (ir[3:0] == 9 || ir[3:0] == 'hb || ir[3:0] == 'hc)
       pf_antic_out = out_reg[7] ? 3'b100 : 3'b000;
     else
       pf_antic_out = 3'b000;

   always @ (posedge clk2_i)
     pf_antic_out_d <= pf_antic_out;

   always @ (vsync or vblank or hblank or dwin or ir or pf_antic_out or
	     pf_antic_out_d) begin
      if (vsync)
	antic_out = 3'b001;
      else if (hblank || vblank)
	if (ir[3:0] == 2 || ir[3:0] == 3 || ir[3:0] == 'hf)
	  antic_out = 3'b011;
	else
	  antic_out = 3'b010;
      else if (dwin)
	antic_out = (dma_block && ir[4] && hscrol[0]) ?
		    pf_antic_out_d : pf_antic_out;
      else
	antic_out = 3'b000;
   end

   always @ (posedge clk_i)
     if (hcount >= 50 && hcount <= 82 && (hcount & 7) == 2)
       refresh_req <= 1;
     else if (refresh_ack)
       refresh_req <= 0;

   assign chr_dcount = dcount ^ {4{chactl[2]}};

   always @ (dl_load or dlist_ctr or load_mis or load_ply or
	     pmbase or vcount or dma_ply_num or
	     load_pf or memscan_ctr or
	     load_char or chbase or shift_reg_out or chr_dcount or
	     refresh_req) begin
      refresh_ack = 0;
      if (dl_load)
	adr_o = dlist_ctr;
      else if (load_mis)
	adr_o = dma_pm_1res ?
		{pmbase[5:1], 3'b011, vcount[7:0]} :
		{pmbase[5:0], 3'b011, vcount[7:1]};
      else if (load_ply)
	adr_o = dma_pm_1res ?
		{pmbase[5:1], 1'b1, dma_ply_num, vcount[7:0]} :
		{pmbase[5:0], 1'b1, dma_ply_num, vcount[7:1]};
      else if (load_pf)
	adr_o = memscan_ctr;
      else if (load_char)
	case (ir[3:0])
	  2: adr_o = {chbase[6:1], shift_reg_out[6:0], chr_dcount[2:0]};
	  3: adr_o = {chbase[6:1], shift_reg_out[6:0], chr_dcount[2:0]};
	  4: adr_o = {chbase[6:1], shift_reg_out[6:0], chr_dcount[2:0]};
	  5: adr_o = {chbase[6:1], shift_reg_out[6:0], chr_dcount[3:1]};
	  6: adr_o = {chbase[6:0], shift_reg_out[5:0], chr_dcount[2:0]};
	  7: adr_o = {chbase[6:0], shift_reg_out[5:0], chr_dcount[3:1]};
	endcase
      else if (refresh_req) begin
	 refresh_ack = 1;
	 adr_o = memscan_ctr;
      end else
	adr_o = memscan_ctr;
   end
   
   assign stb_o = dl_load || load_mis || load_ply || load_pf || load_char ||
		  wsync || refresh_req;
   assign cyc_o = stb_o;

   antic_shift_reg u_shift_reg(.clk_i(clk_i),
			       .shift(shift_reg_shift),
			       .load(load_pf),
			       .in(masterdat_i),
			       .out(shift_reg_out));
endmodule
