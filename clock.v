// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

module clkgen(clk_o, clk2_o);
   output clk_o;
   output clk2_o;

   reg 	  clk_o;
   reg 	  clk2_o;

   initial begin
      clk_o = 0;
      clk2_o = 0;
   end

   always begin
      #5 clk2_o = ~clk2_o;
      #5 clk2_o = ~clk2_o;
      clk_o = ~clk_o;
   end
endmodule