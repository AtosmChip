// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

module gtia_pmunit(clk_i, dat_i, pmdat_i, dmadat_i,
		   hpos_we, size_we, graf_we, grafdma_we,
		   vdelay_we, vdelay,
		   clk2_i,
		   hcount,
		   out);
   parameter size = 8;

   input     clk_i, dat_i, pmdat_i, dmadat_i;
   input     hpos_we, size_we, graf_we, grafdma_we;
   input     vdelay_we, vdelay;
   input     clk2_i;
   input     hcount;

   output    out;

   wire              clk_i;
   wire [7:0] 	     dat_i;
   wire [size - 1:0] pmdat_i, dmadat_i;
   wire 	     hpos_we, size_we, graf_we, grafdma_we;
   wire 	     vdelay_we, vdelay;
   wire 	     clk2_i;
   wire [7:0] 	     hcount;

   wire 	     out;

   reg [7:0] 	     hpos;
   reg [1:0] 	     size;
   reg [size - 1:0]  graf;
   reg [size - 1:0]  shift;

   reg [1:0] 	    cyccnt;
   wire 	    shift_en;

   assign 	    out = shift[size - 1];

   always @ (posedge clk_i) begin
      if (hpos_we)
	hpos <= dat_i;
      if (size_we)
	size <= pmdat_i[1:0];
      if (graf_we)
	graf <= pmdat_i;
      else if (grafdma_we)
	graf <= dmadat_i;
      // TODO: vdelay
   end

   always @ (posedge clk2_i)
     if (hcount == hpos)
       shift <= graf;
     else if (shift_en)
       shift <= {shift[size - 2:0], 1'b0};

   always @ (posedge clk2_i)
     if (hcount == hpos)
       cyccnt <= 0;
     else
       cyccnt <= cyccnt + 1;

   assign shift_en = (size == 2'b00) || (size == 2'b10) ||
		     (size == 2'b01 && cyccnt[0]) ||
		     (size == 2'b11 && cyccnt == 2'b11);

endmodule

module gtia_antic_input(clk2_i,
			nrm, cl9,
			antic_out,
			pf, b, adpl,
			pf2c, pf1lum_sel,
			vsync, blank);
   input clk2_i;
   input nrm, cl9;
   input antic_out;
   output pf, b, adpl;
   output pf2c, pf1lum_sel;
   output vsync, blank;
   
   wire   clk2_i;
   wire   nrm, cl9;
   wire [2:0] antic_out;

   wire [3:0] pf;
   reg [3:0]  b;
   reg [3:0]  adpl;
   wire       pf2c, pf1lum_sel;
   wire       vsync, blank;

   reg 	      chr40l;
   reg 	      even;

   integer    i;

   assign     vsync = (antic_out == 3'b001);
   assign     blank = (antic_out == 3'b010 || antic_out == 3'b011);

   always @ (posedge clk2_i)
     if (!nrm || antic_out == 3'b010)
       chr40l <= 0;
     else if (antic_out == 3'b011)
       chr40l <= 1;
   
   // TODO: replace it with HCOUNT?
   always @ (posedge clk2_i)
     if (blank)
       even <= 1;
     else
       even <= ~even;

   always @ (posedge clk2_i)
     if (even)
       b[3:2] <= antic_out[1:0];
     else
       b[1:0] <= antic_out[1:0];

   assign     pf[0] = (nrm && !chr40l && antic_out == 3'b100) ||
		      (cl9 && b[2:0] == 3'b100);
   assign     pf[1] = (nrm && !chr40l && antic_out == 3'b101) ||
		      (cl9 && b[2:0] == 3'b101);
   assign     pf[2] = (nrm && !chr40l && antic_out == 3'b110) ||
		      (chr40l && antic_out[2]) ||
		      (cl9 && b[2:0] == 3'b110);
   assign     pf[3] = (nrm && !chr40l && antic_out == 3'b111) ||
		      (cl9 && b[2:0] == 3'b111);
   assign     pf2c = (nrm && !chr40l && antic_out == 3'b110) ||
		     (cl9 && b[2:0] == 3'b110) ||
		     (chr40l && (antic_out[1] || antic_out[0]));

   always @ (cl9 or b)
     for (i = 0; i < 4; i = i + 1)
       adpl[i] = cl9 && (b == i);

   assign pf1lum_sel = chr40l && !blank &&
		       (clk2_i ? antic_out[1] : antic_out[0]);
endmodule

module gtia_out(clk_i,
		dat_i,
		colpm_we, colpf_we, colbak_we, gtiactl_we,
		nrm, cl9,
		blank, pl, adpl, mi, pf, pf1lum_sel, b,
		color);
   input clk_i;
   input dat_i;
   input colpm_we, colpf_we, colbak_we, gtiactl_we;
   input blank, pl, adpl, mi, pf, pf1lum_sel, b;

   output nrm, cl9;
   output color;

   wire       clk_i;
   wire [7:0] dat_i;

   wire [3:0] colpm_we;
   wire [3:0] colpf_we;
   wire       colbak_we;
   wire       gtiactl_we;
   
   wire       nrm, cl9, c, lu;

   wire       blank;
   wire [3:0] pl, adpl;
   wire [3:0] mi;
   wire [3:0] pf;
   wire       pf1lum_sel;
   wire [3:0] b;

   reg [7:0] color;

   wire [3:0] pm;
   wire       pf3_p5;

   wire [3:0]  sf, sp;
   wire        sb;

   reg [7:1]  colpf[0:3];
   wire [7:1] colpf1 = colpf[1];
   reg [7:1]  colpm[0:3];
   reg [7:1]  colbak;
   reg [3:0]  prior;
   reg 	      pl5th;
   reg 	      multi_colp;
   reg [1:0]  mode;

   integer    i, si;

   always @ (posedge clk_i) begin
     for (i = 0; i < 4; i = i + 1)
       if (colpm_we[i])
	 colpm[i] <= dat_i[7:1];
       else if (colpf_we[i])
	 colpf[i] <= dat_i[7:1];
      if (colbak_we)
	colbak <= dat_i[7:1];
      if (gtiactl_we) begin
	 prior <= dat_i[3:0];
	 pl5th <= dat_i[4];
	 multi_colp <= dat_i[5];
	 mode <= dat_i[7:6];
      end
   end

   assign pm = pl | adpl | (mi & {4{~pl5th}});

   assign nrm = (mode == 2'b00);
   assign cl9 = (mode == 2'b10) && ! blank;
   assign c = (mode == 2'b11) && !blank && (pm == 0);
   assign lu = (mode == 2'b01) && !blank && (pm == 0);

   assign pf3_p5 = pf[3] || (pl5th && (mi != 0));

   assign sp[0] = !blank && pm[0] && 
		  (!(pf[0] || pf[1]) || (!prior[2] && !prior[3])) &&
		  (!(pf[2] || pf3_p5) || !prior[2]);
   assign sp[1] = !blank && pm[1] &&
		  (!(pf[0] || pf[1]) || (!prior[2] && !prior[3])) &&
		  (!(pf[2] || pf3_p5) || !prior[2]) &&
		  (multi_colp || !pm[0]);
   assign sp[2] = !blank && pm[2] && !(pm[0] || pm[1]) &&
		  (!(pf[0] || pf[1]) || prior[0]) &&
		  (!(pf[2] || pf3_p5) || (!prior[1]) && !prior[2]);
   assign sp[3] = !blank && pm[3] && !(pm[0] || pm[1]) &&
		  (!(pf[0] || pf[1]) || prior[0]) &&
		  (!(pf[2] || pf3_p5) || (!prior[1]) && !prior[2]) &&
		  (multi_colp || !pm[2]);
   
   assign sf[3] = !blank && pf3_p5 &&
		  (!(pm[0] || pm[1]) || prior[2]) &&
		  (!(pm[2] || pm[3]) || (!prior[0] && !prior[3]));
   assign sf[2] = !blank && pf[2] && !sf[3] &&
		  (!(pm[0] || pm[1]) || prior[2]) &&
		  (!(pm[2] || pm[3]) || (!prior[0] && !prior[3]));
   assign sf[1] = !blank && pf[1] && !sf[3] &&
		  (!(pm[0] || pm[1]) || (!prior[0] && !prior[1])) &&
		  (!(pm[2] || pm[3]) || !prior[0]);
   assign sf[0] = !blank && pf[0] && !sf[3] &&
		  (!(pm[0] || pm[1]) || (!prior[0] && !prior[1])) &&
		  (!(pm[2] || pm[3]) || !prior[0]);

   assign sb = !blank && (pf[2:0] == 0) && !pf3_p5 && (pm == 0);

   always @ (sf or sp or sb or pf1lum_sel or b or lu or c or
	     colpf[0] or colpf[1] or colpf[2] or colpf[3] or
	     colpm[0] or colpm[1] or colpm[2] or colpm[3] or colbak) begin
      color = 0;
      for (si = 0; si < 4; si = si + 1) begin
	 if (sp[si])
	   color = color | {colpm[si], 1'b0};
	 if (sf[si])
	   color = color | {colpf[si], 1'b0};
      end
      if (sb)
	color = color | {colbak, 1'b0};
      if (pf1lum_sel)
	color = {color[7:4], colpf1[3:1], 1'b0};
      if (lu)
	color = {color[7:4], b};
      if (c)
	color = {b, color[3:0]};
   end
endmodule

module gtia(rst_i, clk_i,
	    adr_i,
	    dat_i,
	    dat_o,
	    we_i,
	    stb_i,
	    ack_o,
	    clk2_i,
	    dmadat_i,
	    antic_out,
	    color,
	    hsync,
	    vsync,
	    trig_in,
	    consol_in,
	    consol_out);
   input rst_i;
   input clk_i;
   input adr_i;
   input dat_i;
   input we_i;
   input stb_i;
   input clk2_i;
   input dmadat_i;
   input antic_out;
   input trig_in;
   input consol_in;

   output dat_o;
   output ack_o;
   output color;
   output hsync;
   output vsync;
   output consol_out;

   wire       rst_i, clk_i;
   wire [4:0] adr_i;
   wire [7:0] dat_i;
   wire       we_i;
   wire       stb_i;
   wire       clk2_i;
   wire [7:0] dmadat_i;
   wire [2:0] antic_out;
      
   wire       ack_o;
   reg [7:0]  dat_o;

   reg [3:0]  colpm_we;
   reg [3:0]  colpf_we;
   reg 	      colbak_we;
   reg 	      gtiactl_we;
   
   reg [3:0]  hposp_we;
   reg [3:0]  hposm_we;
   reg [3:0]  sizep_we;
   reg 	      sizem_we;
   reg [3:0]  grafp_we;
   reg [3:0]  grafpdma_we;
   reg 	      grafm_we;
   reg 	      grafmdma_we;
   reg 	      vdelay_we;

   reg [3:0]  mxpf[3:0];
   reg [3:0]  pxpf[3:0];
   reg [3:0]  mxpl[3:0];
   reg [3:0]  pxpl[3:0];
   reg 	      hitclr;

   wire [3:0] pf, b;
   wire [3:0] pl, mi, adpl;
   
   wire       blank;
   reg 	      last_blank;
   wire [7:0] color;
   wire       hsync, vsync;

   reg [7:0]  hcount;

   reg 	      dmam, dmap;
   
   wire [3:0] consol_in, consol_out;
   reg [3:0]  consol_out_reg;
   wire [3:0] trig_in;
   reg [3:0]  trig_reg;
   reg 	      trig_latch;

   integer    ki, gi, i;

   assign     ack_o = stb_i;

   // Read registers.
   always @ (adr_i or trig_reg or consol_in or consol_out_reg)
     if (adr_i >= 0 && adr_i < 4)
	dat_o = {4'h0, mxpf[adr_i]};
     else if (adr_i >= 4 && adr_i < 8)
       dat_o = {4'h0, pxpf[adr_i - 4]};
     else if (adr_i >= 8 && adr_i < 'hc)
       dat_o = {4'h0, mxpl[adr_i - 8]};
     else if (adr_i >= 'hc && adr_i < 'h10)
	dat_o = {4'h0, pxpl[adr_i - 'hc]};
     else if (adr_i >= 'h10 && adr_i < 'h14)
       dat_o = {7'h0, trig_reg[adr_i - 'h10]};
     else if (adr_i == 'h14)
       dat_o = 8'h1;  // 'h0f for NTSC
     else if (adr_i == 'h1f)
       dat_o = {4'h0, consol_in &~ consol_out_reg};
     else
       dat_o = 'hff;

   always @ (posedge clk_i)
     if (stb_i && we_i)
       if (adr_i == 'h1d) begin
	  dmam <= dat_i[0];
	  dmap <= dat_i[1];
	  trig_latch <= dat_i[2];
       end else if (adr_i == 'h1f) begin
	  consol_out_reg <= dat_i[3:0];
       end

   always @ (posedge clk_i)
     trig_reg <= trig_latch ? (trig_reg & trig_in) : trig_in;

   always @ (we_i or stb_i or adr_i) begin
      hposp_we = 0;
      hposm_we = 0;
      sizep_we = 0;
      sizem_we = 0;
      grafp_we = 0;
      grafm_we = 0;
      vdelay_we = 0;
      colpm_we = 0;
      colpf_we = 0;
      colbak_we = 0;
      gtiactl_we = 0;
      if (stb_i && we_i)
	if (adr_i >= 0 && adr_i < 4)
	  hposp_we[adr_i] = 1;
	else if (adr_i >= 4 && adr_i < 8)
	  hposm_we[adr_i - 4] = 1;
        else if (adr_i >= 8 && adr_i < 'hc)
	  sizep_we[adr_i - 8] = 1;
	else if (adr_i == 'hc)
	  sizem_we = 1;
	else if (adr_i >= 'hd && adr_i < 'h11)
	 grafp_we[adr_i - 'hd] = 1;
	else if (adr_i == 'h11)
	  grafm_we = 1;
	else if (adr_i >= 'h12 && adr_i < 'h16)
	  colpm_we[adr_i - 'h12] = 1;
	else if (adr_i >= 'h16 && adr_i < 'h1a)
	  colpf_we[adr_i - 'h16] = 1;
	else if (adr_i == 'h1a)
	  colbak_we = 1;
	else if (adr_i == 'h1b)
	  gtiactl_we = 1;
	else if (adr_i == 'h1c)
	  vdelay_we = 1;
   end

   always @ (dmap or dmam or hcount) begin
      // TODO: GTIA count cycles from HALT (see datasheet).
      for (gi = 0; gi < 4; gi = gi + 1)
	grafpdma_we[gi] = dmap && (hcount == 4 + 2 * gi);
      grafmdma_we = dmam && (hcount == 0);
   end

   assign     hsync = (hcount < 16);

   gtia_antic_input u_antic_input(clk2_i, nrm, cl9,
				  antic_out,
				  pf, b, adpl,
				  pf2c, pf1lum_sel,
				  vsync, blank);

   // Collisions.
   always @ (posedge clk_i)
     if (stb_i && we_i && adr_i == 'h1e)
       hitclr <= 1;
     else
       hitclr <= 0;

   // TODO: pf2c
   always @ (posedge clk2_i)
     if (hitclr)
       for (ki = 0; ki < 4; ki = ki + 1) begin
	  mxpf[ki] <= 0;
	  pxpf[ki] <= 0;
	  mxpl[ki] <= 0;
	  pxpl[ki] <= 0;
       end
     else
       for (ki = 0; ki < 4; ki = ki + 1) begin
	  if (mi[ki]) begin
	     mxpf[ki] <= mxpf[ki] | pf[ki];
	     mxpl[ki] <= mxpl[ki] | pl[ki];
	  end
	  if (pl[ki]) begin
	     pxpf[ki] <= pxpf[ki] | pf[ki];
	     pxpl[ki] <= pxpl[ki] | (pl[ki] & ~(1 << ki));
	  end
       end

   always @ (posedge clk2_i)
     last_blank <= blank;

   always @ (posedge clk2_i)
     if (!last_blank && blank)
       hcount <= 223;
     else if (hcount == 227)
       hcount <= 0;
     else
       hcount <= hcount + 1;

   gtia_out u_out(.clk_i(clk_i), .dat_i(dat_i),
		  .colpm_we(colpm_we), .colpf_we(colpf_we),
		  .colbak_we(colbak_we), .gtiactl_we(gtiactl_we),
		  .nrm(nrm), .cl9(cl9),
		  .blank(blank), .pl(pl), .adpl(adpl), .mi(mi), .pf(pf), .b(b),
		  .pf1lum_sel(pf1lum_sel),
		  .color(color));

   gtia_pmunit u_pl0(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i),
		     .dmadat_i(dmadat_i),
		     .hpos_we(hposp_we[0]), .size_we(sizep_we[0]),
		     .graf_we(grafp_we[0]), .grafdma_we(grafpdma_we[0]),
		     .vdelay_we(vdelay_we), .vdelay(dat_i[4]),
		     .clk2_i(clk2_i), .hcount(hcount), .out(pl[0]));
   gtia_pmunit u_pl1(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i),
		     .dmadat_i(dmadat_i),
		     .hpos_we(hposp_we[1]), .size_we(sizep_we[1]),
		     .graf_we(grafp_we[1]), .grafdma_we(grafpdma_we[1]),
		     .vdelay_we(vdelay_we), .vdelay(dat_i[5]),
		     .clk2_i(clk2_i), .hcount(hcount), .out(pl[1]));
   gtia_pmunit u_pl2(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i),
		     .dmadat_i(dmadat_i),
		     .hpos_we(hposp_we[2]), .size_we(sizep_we[2]),
		     .graf_we(grafp_we[2]), .grafdma_we(grafpdma_we[2]),
		     .vdelay_we(vdelay_we), .vdelay(dat_i[6]),
		     .clk2_i(clk2_i), .hcount(hcount), .out(pl[2]));
   gtia_pmunit u_pl3(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i),
		     .dmadat_i(dmadat_i),
		     .hpos_we(hposp_we[3]), .size_we(sizep_we[3]),
		     .graf_we(grafp_we[3]), .grafdma_we(grafpdma_we[3]),
		     .vdelay_we(vdelay_we), .vdelay(dat_i[7]),
		     .clk2_i(clk2_i), .hcount(hcount), .out(pl[3]));
   
   defparam   u_m0.size = 2;
   defparam   u_m1.size = 2;
   defparam   u_m2.size = 2;
   defparam   u_m3.size = 2;
   gtia_pmunit u_m0(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i[1:0]),
		    .dmadat_i(dmadat_i[1:0]),
		    .hpos_we(hposm_we[0]), .size_we(sizem_we),
		    .graf_we(grafm_we), .grafdma_we(grafmdma_we),
		    .vdelay_we(vdelay_we), .vdelay(dat_i[0]),
		    .clk2_i(clk2_i), .hcount(hcount), .out(mi[0]));
   gtia_pmunit u_m1(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i[3:2]),
		    .dmadat_i(dmadat_i[3:2]),
		    .hpos_we(hposm_we[1]), .size_we(sizem_we),
		    .graf_we(grafm_we), .grafdma_we(grafmdma_we),
		    .vdelay_we(vdelay_we), .vdelay(dat_i[1]),
		    .clk2_i(clk2_i), .hcount(hcount), .out(mi[1]));
   gtia_pmunit u_m2(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i[5:4]),
		    .dmadat_i(dmadat_i[5:4]),
		    .hpos_we(hposm_we[2]), .size_we(sizem_we),
		    .graf_we(grafm_we), .grafdma_we(grafmdma_we),
		    .vdelay_we(vdelay_we), .vdelay(dat_i[2]),
		    .clk2_i(clk2_i), .hcount(hcount), .out(mi[2]));
   gtia_pmunit u_m3(.clk_i(clk_i), .dat_i(dat_i), .pmdat_i(dat_i[7:6]),
		    .dmadat_i(dmadat_i[7:6]),
		    .hpos_we(hposm_we[3]), .size_we(sizem_we),
		    .graf_we(grafm_we), .grafdma_we(grafmdma_we),
		    .vdelay_we(vdelay_we), .vdelay(dat_i[3]),
		    .clk2_i(clk2_i), .hcount(hcount), .out(mi[3]));

   assign     consol_out = ~consol_out_reg;

endmodule
