;; Atosm Chip
;; Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

atserout = $8000
atserin = $8001
atserstat = $8002
atserstat_inempty = 1
atserstat_outrdy = 2
atserstat_cmd = 4

.macro	debug n
	pha
	lda #n
	sta $9000
	pla
.endmacro

	.code
start:	ldx #$ff
	txs

	lda #1
	sta serindone
	lda #1
	sta seroutdone

	debug $11
@cmdl:	lda atserstat
	and #atserstat_cmd
	beq @cmdl
	
	debug $12
@cmdh:	lda atserstat
	and #atserstat_cmd
	bne @cmdh

	debug $13

	lda #4
	sta serinlen
	lda #<serinbuf
	sta serinadr
	lda #>serinbuf
	sta serinadr+1
	lda #0
	sta serinpos
	lda #0
	sta serindone

	debug $14

@1:	jsr events
	lda serindone
	beq @1

	debug $15

	;; try "wait until cmd=h" instead of delay
	jsr delay

	lda #'A'
	sta atserin

	jsr delay
	lda #'C'
	sta atserin

	jsr delay
	lda #0
	sta atserin

	jsr delay
	lda #0
	sta atserin

	jsr delay
	lda #$e0
	sta atserin

	jsr delay
	lda #0
	sta atserin

	jsr delay
	lda #$e0
	sta atserin

@2:	jsr events
	lda atserstat
	and atserstat_inempty
	beq @2

wait:	jmp start
	
events:	lda atserstat
	and #atserstat_outrdy
	beq @1
	jsr seroutrdy

@1:	lda atserstat
	and #atserstat_inempty
	beq @2
	jsr serinempty	

@2:	rts

serinempty:
	lda seroutdone
	bne @ign
	jsr seroutsend
@ign:	rts

seroutsend:
	ldy seroutpos
	lda (seroutadr),y
	sta atserin
	iny
	sty seroutpos
	cpy seroutlen
	bcc @nend
	lda #1
	sta seroutdone
@nend:	rts

seroutrdy:
	debug $22
	lda serindone
	bne @ign
	debug $23
	ldy serinpos
	cpy serinlen
	beq @cksum
	debug $24
	bcs @ign
	debug $25
	lda atserout
	sta (serinadr),y
	debug $26
	iny
	sty serinpos
@ign:	rts
@cksum:	lda atserout
	sta serincksum
	;; TODO: check the checksum
	debug $27
	lda #1
	sta serindone
	rts

delay:	ldy #30
@d2:	ldx #0
@d1:	dex
	bne @d1
	dey
	bne @d2
	rts


nmi:	rti
irq:	rti

	.zeropage
serinadr:	.res 2
seroutadr:	.res 2

	.bss
serinpos:	.res 1
serinlen:	.res 1		; TODO:	how to deal with 256 byte records?
serincksum:	.res 1
serindone:	.res 1

seroutpos:	.res 1
seroutlen:	.res 1		; TODO:	how to deal with 256 byte records?
seroutdone:	.res 1

serinbuf:	.res 256

	.segment "VECTORS"
	.word nmi
	.word start
	.word irq
