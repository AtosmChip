// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "memory.v"

module ram_tb();
   reg        clk_o;
   reg [15:0] adr_o;
   reg [7:0]  dat_o;
   reg 	      rst_o;
   reg 	      stb_o;
   reg 	      we_o;

   wire       ack_i;
   wire [7:0] dat_i;

   initial begin
      $readmemh("rom.list", u_memory.u_rom.memory);

      $display("time\t clk rst stb we  adr              do       di");
      $monitor("%g\t %b   %b   %b   %b   %b %b %b",
	       $time, clk_o, rst_o, stb_o, we_o, adr_o, dat_o, dat_i);
      clk_o = 1;
      rst_o = 0;
      stb_o = 0;
      we_o = 0;
      #5 rst_o = 1;

      #10 rst_o = 0;
      we_o = 1;
      stb_o = 1;
      adr_o = 'h10;
      dat_o = 'h55;

      #10 adr_o = 'h20;
      dat_o = 'haa;

      #10 we_o = 0;
      adr_o = 'h10;

      #10 adr_o = 'h20;
      
      #10 adr_o = 'hfc00;
      #10 adr_o = 'hfc01;
      #10 adr_o = 'hfc02;
      #10 adr_o = 'hfc03;
      #10 adr_o = 'hfc04;

      #10 $finish;
   end

   always begin
      #5 clk_o = ~clk_o;
   end
   
   memory u_memory(.clk_i(clk_o),
		   .adr_i(adr_o),
		   .dat_i(dat_o),
		   .rst_i(rst_o),
		   .stb_i(stb_o),
		   .we_i(we_o),
		   .ack_o(ack_i),
		   .dat_o(dat_i));

endmodule // ram_tb
