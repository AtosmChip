// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "clock.v"
`include "atari.v"

module atari_tb();
   wire clk;
   wire clk2;

   reg rst_o;

   reg [7:0] key_code;
   reg       key_pressed;
   reg       key_shift, key_break;
   reg       key_start, key_select, key_option;
   reg [7:0] joystick;
   reg [1:0] trig;

   parameter monitor = 0;

   initial begin
      $readmemh("os/atarios.memh", u_atari.u_rom.memory);

      key_code = 0;
      key_pressed = 0;
      key_shift = 0;
      key_break = 0;
      key_start = 0;
      key_select = 0;
      key_option = 0;
      joystick = 0;
      trig = 0;

      if (monitor)
	$monitor("%g\t pc=%h a=%h x=%h y=%h f=%b s=%h",
		 $time,
		 u_atari.u_cpu.pc,
		 u_atari.u_cpu.acc,
		 u_atari.u_cpu.regx,
		 u_atari.u_cpu.regy,
		 u_atari.u_cpu.regf,
		 u_atari.u_cpu.regs);

      rst_o = 0;
      #5 rst_o = 1;
      #40 rst_o = 0;
      #2_000_000_0;
      $writememh("ram.memh", u_atari.u_ram.memory);
      $finish;
   end

   always @ (posedge clk) begin
      if (u_atari.adr >= 'hd000 && u_atari.adr < 'hd800) begin
	 if (u_atari.we)
	   $display("hardware write %h = %h at %h",
		    u_atari.adr, u_atari.masterdat_o, u_atari.u_cpu.pc);
	 else
	   $display("hardware read  %h = %h at %h",
		    u_atari.adr, u_atari.slavedat_o, u_atari.u_cpu.pc);
      end
   end

   clkgen u_clkgen(.clk_o(clk), .clk2_o(clk2));
   atari u_atari(.clk_i(clk), .clk2_i(clk2), .rst_i(rst_o),
		 .key_code(key_code),
		 .key_pressed(key_pressed),
		 .key_shift(key_shift), .key_break(key_break),
		 .key_start(key_start), .key_select(key_select),
		 .key_option(key_option),
		 .joystick(joystick), .trig(trig));

endmodule
