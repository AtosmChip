// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// TODO: separate decoding of addressing mode and execution of instruction
// TODO: constants for opcodes
// TODO: on RESET, stb_o and cyc_o=0 (Wishbonem p.38)

module cpu6502_alu(a, b, op, out, cin, cout, vout, d);
   input a;
   input b;
   input cin;
   input d;
   input op;

   output out;
   output cout;
   output vout;

   wire [3:0] op;
   wire [7:0] a;
   wire [7:0] b;
   wire       cin;
   wire       d;
   reg [7:0]  out;
   reg 	      cout, vout;
   
   parameter [3:0] OPOR  = 4'b0000;
   parameter [3:0] OPAND = 4'b0001;
   parameter [3:0] OPEOR = 4'b0010;
   parameter [3:0] OPADC = 4'b0011;
   parameter [3:0] OPA   = 4'b0100;
   parameter [3:0] OPB   = 4'b0101;
   parameter [3:0] OPCMP = 4'b0110;
   parameter [3:0] OPSBC = 4'b0111;
   parameter [3:0] OPASL = 4'b1000;
   parameter [3:0] OPROL = 4'b1001;
   parameter [3:0] OPLSR = 4'b1010;
   parameter [3:0] OPROR = 4'b1011;
   parameter [3:0] OPDEC = 4'b1110;
   parameter [3:0] OPINC = 4'b1111;
	      
   always @ (a or b or op or cin or d) begin
      cout = 0;  // don't care
      vout = 0;
      // TODO: v, decimal mode
      case (op)
	OPOR:  out = a | b;
	OPAND: out = a & b;
	OPEOR: out = a ^ b;
	OPADC: {cout, out} = {1'b0, a} + {1'b0, b} + cin;
	OPA:   out = a;
	OPB:   out = b;
	OPCMP: {cout, out} = {1'b0, a} + {1'b0, ~b} + 1;
	OPSBC: {cout, out} = {1'b0, a} + {1'b0, ~b} + cin;
	OPASL: {cout, out} = {a, 1'b0};
	OPROL: {cout, out} = {a, cin};
	OPLSR: {out, cout} = {1'b0, a};
	OPROR: {out, cout} = {cin, a};
	OPINC: out = a + 1;
	OPDEC: out = a - 1;
	default: begin
	   out = 0;   // don't care
	end
      endcase
   end
   
endmodule // cpu6502_alu

module cpu6502_adr(clk, rst, halt, dat_i,
		   regx, regy, regs, pc, dreg,
		   adro_sel, indx_sel, base_sel, zp,
		   interrupt,
		   low_ld, high_ld,
		   adr_c,
		   adr_out);
   input clk, rst, halt, dat_i;
   input regx, regy, regs, pc, dreg;
   input adro_sel, indx_sel, base_sel, zp;
   input interrupt;
   input low_ld, high_ld;
   output adr_out;
   output adr_c;
   
   wire   clk;
   wire   rst;
   wire   halt;
   wire [7:0] dat_i;
   wire [7:0] regx;
   wire [7:0] regy;
   wire [7:0] regs;
   wire [15:0] pc;
   wire [7:0]  dreg;
   wire [2:0]  adro_sel;
   wire        indx_sel;
   wire        base_sel;
   wire        zp;
   wire [1:0]  interrupt;
   wire        low_ld, high_ld;

   reg [15:0]  adr_out;
   wire	       adr_c;

   parameter [2:0] ADRO_PC      = 3'b000;
   parameter [2:0] ADRO_BASE    = 3'b001;
   parameter [2:0] ADRO_BASEIND = 3'b010;
   parameter [2:0] ADRO_SP      = 3'b011;
   parameter [2:0] ADRO_VECL    = 3'b100;
   parameter [2:0] ADRO_VECH    = 3'b101;
   parameter [2:0] ADRO_BASE1   = 3'b110;
   
   reg  [15:0] areg;
   wire [7:0]  ind;
   wire [7:0]  base;
   wire [8:0]  sum;

   assign      ind = indx_sel ? regx : regy;
   assign      base = base_sel ? dreg : areg[7:0];
   assign      sum = base + ind;
   assign      adr_c = sum[8];
      
   always @ (adro_sel or pc or zp or areg or base or sum or regs or
	     interrupt) begin
      case (adro_sel)
	ADRO_PC:   adr_out = pc;
	ADRO_BASE: begin
	   adr_out[15:8] = zp ? 0 : areg[15:8];
	   adr_out[7:0] = base[7:0];
	end
	ADRO_BASE1: begin
	   adr_out[15:8] = zp ? 0 : areg[15:8];
	   adr_out[7:0] = base[7:0] + 1;
	end
	ADRO_BASEIND: begin
	   adr_out[15:8] = zp ? {7'h0, sum[8]} : areg[15:8] + sum[8];
	   adr_out[7:0] = sum[7:0];
	end
	ADRO_SP: begin
	   adr_out[15:8] = 1;
	   adr_out[7:0] = regs;
	end
	ADRO_VECL: adr_out = { 12'hfff, 1'b1, interrupt, 1'b0 };
	ADRO_VECH: adr_out = { 12'hfff, 1'b1, interrupt, 1'b1 };
	default:   adr_out = pc;  // don't care
      endcase
   end

   always @ (posedge clk) begin
      if (!halt) begin
	 if (low_ld == 1)
	   areg[7:0] <= dat_i;
	 if (high_ld == 1)
	   areg[15:8] <= dat_i;
      end
   end

endmodule // cpu6502_adr


module cpu6502(rst_i,
	       clk_i,
	       adr_o,
	       dat_i,
	       dat_o,
	       we_o,
	       stb_o,
	       ack_i,
	       cyc_o,
	       nmi,
	       irq);
   input rst_i;
   input clk_i;
   input dat_i;
   input ack_i;
   output adr_o;
   output dat_o;
   output we_o;
   output stb_o;
   output cyc_o;
   input  nmi;
   input  irq;

   wire       rst_i;
   wire       clk_i;
   wire [7:0] dat_i;
   wire       ack_i;
   wire [15:0] adr_o;
   reg [7:0]  dat_o;
   reg 	      we_o;
   wire       stb_o;
   wire       cyc_o;
   wire       nmi;
   wire       irq;

   reg [15:0]  pc;
   reg 	       pcinc;
   reg 	       pc_low_ld;
   reg 	       pc_high_ld;
   reg 	       pc_rel_low_ld;
   reg 	       pc_rel_high_ld;
   reg         pc_dreg_ld;
   wire        pc_c;
   wire [7:0]  pc_rel_low;

   parameter   N_IND = 5;
   parameter   V_IND = 4;
   parameter   D_IND = 3;
   parameter   I_IND = 2;
   parameter   Z_IND = 1;
   parameter   C_IND = 0;

   wire        halt;

   reg [7:0]   ir;
   reg [7:0]   acc;
   reg [7:0]   regx;
   reg [7:0]   regy;
   reg [7:0]   regs;
   reg [5:0]   regf;
   reg [7:0]   dreg;
   reg [2:0]   cyccnt;
   reg [1:0]   rmwcyccnt;

   reg [1:0]   interrupt;

   parameter [1:0] INT_NONE = 2'b00;
   parameter [1:0] INT_NMI  = 2'b01;
   parameter [1:0] INT_RES  = 2'b10;
   parameter [1:0] INT_IRQ  = 2'b11;

   reg 		   last_nmi;
   reg 		   nmi_pending;

   parameter [3:0] MODE_IMP  = 4'b0000;
   parameter [3:0] MODE_IMM  = 4'b0001;
   parameter [3:0] MODE_INDX = 4'b0010;
   parameter [3:0] MODE_INDY = 4'b0011;
   parameter [3:0] MODE_ZP   = 4'b0100;
   parameter [3:0] MODE_ABS  = 4'b0101;
   parameter [3:0] MODE_ZPX  = 4'b0110;
   parameter [3:0] MODE_ZPY  = 4'b0111;
   parameter [3:0] MODE_ABSX = 4'b1000;
   parameter [3:0] MODE_ABSY = 4'b1001;
   
   parameter [3:0] INSTR_ALU   = 4'b0000;
   parameter [3:0] INSTR_RMW   = 4'b0001;
   parameter [3:0] INSTR_STA   = 4'b0010;
   parameter [3:0] INSTR_STXY  = 4'b0011;
   parameter [3:0] INSTR_LDXY  = 4'b0100;
   parameter [3:0] INSTR_CPXY  = 4'b0101;
   parameter [3:0] INSTR_BIT   = 4'b0110;
   parameter [3:0] INSTR_OTHER = 4'b1111;
   
   reg [2:0]   adro_sel;
   reg 	       adr_indx_sel;
   reg         adr_base_sel;
   reg 	       adr_zp;
   reg 	       adr_low_ld;
   reg 	       adr_high_ld;
   wire        adr_c;
 	       
   reg 	       br_flag;
   reg 	       br_taken;

   reg [3:0]   adr_mode;
   reg [3:0]   instr_type;
   reg 	       adr_mode_done;
	       
   reg 	       irld, accld;
   reg 	       lastcyc, skipcyc, inc_rmwcyccnt;
   reg         regxld, regyld, regs_ld;
   reg         dreg_ld;
   reg         nld, zld, cld, vld, fld;
   reg [2:0]   dato_sel;

   parameter [3:0] ALUA_A = 3'b000;
   parameter [3:0] ALUA_X = 3'b001;
   parameter [3:0] ALUA_Y = 3'b010;
   parameter [3:0] ALUA_S = 3'b011;
   parameter [3:0] ALUA_D = 3'b100;

   reg [3:0]   alua_sel;
   reg [3:0]   aluop;
   reg [7:0]   alua;
   wire [7:0]  alub;
   wire [7:0]  aluout;
   wire        alucout, alud, aluvout;

   parameter [2:0] DATO_ACC = 3'b000;
   parameter [2:0] DATO_PCH = 3'b001;
   parameter [2:0] DATO_PCL = 3'b010;
   parameter [2:0] DATO_FLG = 3'b011; 
   parameter [2:0] DATO_X   = 3'b100;
   parameter [2:0] DATO_Y   = 3'b101;
   parameter [2:0] DATO_D   = 3'b110;

   assign      cyc_o = 1;
   assign      stb_o = 1;

   assign      halt = stb_o && !ack_i;

   always @ (alua_sel or acc or regx or regy or regs or dreg) begin
      case (alua_sel)
	ALUA_A: alua = acc;
	ALUA_X: alua = regx;
	ALUA_Y: alua = regy;
	ALUA_S: alua = regs;
	ALUA_D: alua = dreg;
	default: alua = acc;
      endcase
   end
	       
   assign      alub = dat_i;

   always @ (ir or regf) begin
      case (ir[7:6])
	2'b00: br_flag = regf[N_IND];
	2'b01: br_flag = regf[V_IND];
	2'b10: br_flag = regf[C_IND];
	2'b11: br_flag = regf[Z_IND];
      endcase
      br_taken = (br_flag == ir[5]);
   end

   always @ (ir) begin
      adr_mode = MODE_IMP;
      if (ir == 'h4c || ir == 'h6c || ir == 'h20 || ir == 'h40 ||
	  ir == 'h60 || ir[4:0] == 'h10)
	adr_mode = MODE_IMP;
      else if (ir[4:0] == 'h00) begin
	 adr_mode = MODE_IMM;
      end else if (ir[4:0] == 'h01) begin
	 adr_mode = MODE_INDX;
      end else if (ir[4:0] == 'h02) begin
	 adr_mode = MODE_IMM;
      end else if (ir[4:0] == 'h04 || ir[4:0] == 'h05 || ir[4:0] == 'h06) begin
	 adr_mode = MODE_ZP;
      end else if (ir[4:0] == 'h08) begin
	 adr_mode = MODE_IMP;
      end else if (ir[4:0] == 'h09) begin
	 adr_mode = MODE_IMM;
      end else if (ir[4:0] == 'h0a) begin
	 adr_mode = MODE_IMP;
      end else if (ir[4:0] == 'h0c || ir[4:0] == 'h0d || ir[4:0] == 'h0e) begin
	 adr_mode = MODE_ABS;
      end else if (ir[4:0] == 'h11) begin
	 adr_mode = MODE_INDY;
      end else if (ir[4:0] == 'h14 || ir[4:0] == 'h15 || ir[4:0] == 'h16) begin
	 if (ir == 'h96 || ir == 'hb6)
	   adr_mode = MODE_ZPY;
	 else
	   adr_mode = MODE_ZPX;
      end else if (ir[4:0] == 'h19) begin
	 adr_mode = MODE_ABSY;
      end else if (ir[4:0] == 'h1c || ir[4:0] == 'h1d || ir[4:0] == 'h1e) begin
	 if (ir == 'hbe)
	   adr_mode = MODE_ABSY;
	 else
	   adr_mode = MODE_ABSX;
      end
   end
		   
   always @ (ir) begin
      instr_type = INSTR_OTHER;
      if (ir == 'h89 || ir == 'h24 || ir == 'h2c)
	instr_type = INSTR_BIT;
      else if (ir[1:0] == 'h01) begin
	 if (ir[7:5] == 3'b100)
	   instr_type = INSTR_STA;
	 else
	   instr_type = INSTR_ALU;
      end else if (ir == 'h84 || ir == 'h86 || ir == 'h8c || ir == 'h8e ||
		   ir == 'h94 || ir == 'h96)
	instr_type = INSTR_STXY;
      else if (ir == 'ha0 || ir == 'ha2 || ir == 'ha4 || ir == 'ha6 ||
	       ir == 'hac || ir == 'hae ||
	       ir == 'hb4 || ir == 'hb6 || ir == 'hbc || ir == 'hbe)
	instr_type = INSTR_LDXY;
      else if (ir == 'hc0 || ir == 'hc4 || ir == 'hcc ||
	       ir == 'he0 || ir == 'he4 || ir == 'hec)
	instr_type = INSTR_CPXY;
      else if (ir[2:0] == 'h06)
	instr_type = INSTR_RMW;
   end

   // TODO: look at this list
   always @ (interrupt or cyccnt or rmwcyccnt or ir or br_taken or
	     pc_c or dreg or adr_mode or instr_type or adr_c) begin
      we_o    = 0;
      irld    = 0;
      accld   = 0;
      regxld  = 0;
      regyld  = 0;
      regs_ld = 0;
      nld     = 0;
      zld     = 0;
      cld     = 0;
      vld     = 0;
      fld     = 0;
      dreg_ld = 0;
      
      pcinc   = 1;
      pc_low_ld = 0;
      pc_high_ld = 0;
      pc_rel_low_ld = 0;
      pc_rel_high_ld = 0;
      pc_dreg_ld = 0;

      lastcyc = 0;
      skipcyc = 0;
      inc_rmwcyccnt = 0;

      adro_sel = 0;
      adr_indx_sel = 0;
      adr_base_sel = 0;
      adr_zp = 0;
      adr_low_ld  = 0;
      adr_high_ld = 0;

      dato_sel = 0;

      aluop   = 0;  // don't care
      alua_sel = ALUA_A;

      adr_mode_done = 0;

      if (cyccnt == 0) begin
	 adro_sel = adr.ADRO_PC;
	 irld = 1;
	 pcinc = (interrupt == INT_NONE);
      end else if (ir == 'h00 || interrupt != INT_NONE)
	case (cyccnt)
	  1: pcinc = 0;
	  2: begin
	     pcinc = 0;
	     adro_sel = adr.ADRO_SP;
	     aluop = alu.OPDEC;
	     alua_sel = ALUA_S;
	     regs_ld = 1;
	     dato_sel = DATO_PCH;
	     we_o = 1;
	  end
	  3: begin
	     pcinc = 0;
	     adro_sel = adr.ADRO_SP;
	     aluop = alu.OPDEC;
	     alua_sel = ALUA_S;
	     regs_ld = 1;
	     dato_sel = DATO_PCL;
	     we_o = 1;
	  end
	  4: begin
	     pcinc = 0;
	     adro_sel = adr.ADRO_SP;
	     aluop = alu.OPDEC;
	     alua_sel = ALUA_S;
	     regs_ld = 1;
	     dato_sel = DATO_FLG;
	     we_o = 1;
	  end
	  5: begin
	     pcinc = 0;
	     adro_sel = adr.ADRO_VECL;
	     pc_low_ld = 1;
	  end
	  6: begin
	     pcinc = 0;
	     adro_sel = adr.ADRO_VECH;
	     pc_high_ld = 1;
	     lastcyc = 1;
	  end
	endcase
      else if (adr_mode == MODE_INDX) begin
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      // TODO: add X to DREG here?
	      adro_sel = adr.ADRO_BASE;
	      adr_zp = 1;
	      adr_base_sel = 1;
	      pcinc = 0;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_zp = 1;
	      adr_base_sel = 1;
	      adr_indx_sel = 1;
	      pcinc = 0;
	      adr_low_ld = 1;
	      alua_sel = ALUA_D;
	      aluop = alu.OPINC;
	      dreg_ld = 1;
	   end
	   4: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_zp = 1;
	      adr_base_sel = 1;
	      adr_indx_sel = 1;
	      pcinc = 0;
	      adr_high_ld = 1;
	   end
	   5: begin
	      adro_sel = adr.ADRO_BASE;
	      pcinc = 0;
	      adr_mode_done = 1;
	   end
	 endcase
      end else if (adr_mode == MODE_ZP) begin
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      adr_low_ld = 1;
	   end
	   default: begin
	      adro_sel = adr.ADRO_BASE;
	      adr_zp = 1;
	      adr_mode_done = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (adr_mode == MODE_IMM) begin
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      adr_mode_done = 1;
	   end
	 endcase
      end else if (adr_mode == MODE_ABS) begin
	 case (cyccnt)
	   1: adr_low_ld  = 1;
	   2: adr_high_ld = 1;
	   default: begin
	      adro_sel = adr.ADRO_BASE;
	      adr_mode_done = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (adr_mode == MODE_INDY) begin
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = adr.ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      pcinc = 0;
	      adr_low_ld = 1;
	      alua_sel = ALUA_D;
	      aluop = alu.OPINC;
	      dreg_ld = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      pcinc = 0;
	      adr_high_ld = 1;
	      adr_indx_sel = 0;
	   end
	   // TODO adr_c delay?
	   4: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = 0;
	      pcinc = 0;
	      adr_mode_done = 1;
	   end
	 endcase
      end else if (adr_mode == MODE_ZPX || adr_mode == MODE_ZPY) begin
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      adr_low_ld = 1;
	   end
	   2: begin
	      adro_sel = adr.ADRO_BASE;
	      adr_zp = 1;
	      pcinc = 0;
	   end
	   default: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_zp = 1;
	      adr_indx_sel = (adr_mode == MODE_ZPX);
	      pcinc = 0;
	      adr_mode_done = 1;
	   end
	 endcase
      end else if (adr_mode == MODE_ABSX || adr_mode == MODE_ABSY) begin
	 case (cyccnt)
	   1: begin
	      adr_low_ld  = 1;
	   end
	   2: begin
	      adr_high_ld = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = (adr_mode == MODE_ABSX);
	      // TODO what about RMW?
	      adr_mode_done = (adr_c || instr_type == INSTR_STA ||
			       instr_type == INSTR_STXY) ? 0 : 1;
	      pcinc = 0;
	   end
	   default: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = (adr_mode == MODE_ABSX);
	      adr_mode_done = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir == 'h4c) begin
	 // JMP a
	 case (cyccnt)
	   1: begin
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      pc_high_ld = 1;
	      pc_dreg_ld = 1;
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h6c) begin
	 // JMP (a)
	 case (cyccnt)
	   1: adr_low_ld  = 1;
	   2: adr_high_ld = 1;
	   3: pcinc = 0;
	   4: begin
	      adro_sel = adr.ADRO_BASE;
	      pcinc = 0;
	      pc_low_ld = 1;
	   end
	   5: begin
	      adro_sel = adr.ADRO_BASE1;
	      pcinc = 0;
	      pc_high_ld = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h20) begin
	 // JSR a
	 case (cyccnt)
	   1: begin
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = adr.ADRO_SP;
	      pcinc = 0;
	   end
	   3: begin
	      pcinc = 0;
	      adro_sel = adr.ADRO_SP;
	      aluop = alu.OPDEC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	      dato_sel = DATO_PCH;
	      we_o = 1;
	   end
	   4: begin
	      pcinc = 0;
	      adro_sel = adr.ADRO_SP;
	      aluop = alu.OPDEC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	      dato_sel = DATO_PCL;
	      we_o = 1;
	   end
	   5: begin
	      pc_high_ld = 1;
	      pc_dreg_ld = 1;
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h40 || ir == 'h60) begin
	 // RTI/RTS
	 case (cyccnt)
	   1: begin
	      pcinc = 0;
	      aluop = alu.OPINC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	   end
	   2: begin
	      pcinc = 0;
	      aluop = alu.OPINC;
	      alua_sel = ALUA_S;
	      regs_ld = (ir == 'h40);
	      adro_sel = (ir == 'h40) ? adr.ADRO_SP : adr.ADRO_PC;
	      fld = (ir == 'h40);
	   end
	   3: begin
	      pcinc = 0;
	      pc_low_ld = 1;
	      adro_sel = adr.ADRO_SP;
	      aluop = alu.OPINC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	   end
	   4: begin
	      pcinc = 0;
	      pc_high_ld = 1;
	      adro_sel = adr.ADRO_SP;
	   end
	   5: begin
	      lastcyc = 1;
	      pcinc = (ir == 'h60);
	   end
	 endcase
      end else if (ir[4:0] == 'h10) begin
	 // Branch
	 case (cyccnt)
	   1: begin
	      aluop = alu.OPB;
	      adro_sel = adr.ADRO_PC;
	      if (br_taken)
		dreg_ld = 1;
	      else
		lastcyc = 1;
	   end
	   2: begin
	      pcinc = 0;
	      pc_rel_low_ld = 1;
	      lastcyc = (pc_c & dreg[7]) | (~pc_c & ~dreg[7]);
	   end
	   3: begin
	      pcinc = 0;
	      pc_rel_high_ld = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h48 || ir == 'h08) begin
	 // PHA/PHP
	 case (cyccnt)
	   1: pcinc = 0;
	   2: begin
	      pcinc = 0;
	      adro_sel = adr.ADRO_SP;
	      aluop = alu.OPDEC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	      dato_sel = (ir == 'h48) ? DATO_ACC : DATO_FLG;
	      we_o = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h68 || ir == 'h28) begin
	 // PLA/PLP
	 case (cyccnt)
	   1: begin
	      pcinc = 0;
	      aluop = alu.OPINC;
	      alua_sel = ALUA_S;
	      regs_ld = 1;
	   end
	   2: pcinc = 0;
	   3: begin
	      pcinc = 0;
	      aluop = alu.OPB;
	      accld = (ir == 'h68);
	      fld = (ir == 'h28);
	      adro_sel = adr.ADRO_SP;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h18 || ir == 'h38 || ir == 'h58 || ir == 'h78 ||
		   ir == 'hb8 || ir == 'hd8 || ir == 'hf8) begin
	 // CLx / SEx
	 // Everything is done in regf process.
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'h0a || ir == 'h2a || ir == 'h4a || ir == 'h6a) begin
	 // RMW A
	 aluop = {1'b1, ir[7:5]};
	 accld = 1;
	 cld = 1;
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'hca || ir == 'h88) begin
	 // DEX/DEY
	 aluop = alu.OPDEC;
	 regxld = (ir == 'hca);
	 regyld = (ir == 'h88);
	 alua_sel = (ir == 'hca) ? ALUA_X : ALUA_Y;
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'he8 || ir == 'hc8) begin
	 // INX/INY
	 aluop = alu.OPINC;
	 regxld = (ir == 'he8);
	 regyld = (ir == 'hc8);
	 alua_sel = (ir == 'he8) ? ALUA_X : ALUA_Y;
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'h8a || ir == 'h98) begin
	 // TXA/TYA
	 aluop = alu.OPA;
	 accld = 1;
	 alua_sel = (ir == 'h8a) ? ALUA_X : ALUA_Y;
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'haa || ir == 'ha8) begin
	 // TAX/TAY
	 aluop = alu.OPA;
	 alua_sel = ALUA_A;
	 regxld = (ir == 'haa);
	 regyld = (ir == 'ha8);
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'h9a) begin
	 // TXS
	 aluop = alu.OPA;
	 alua_sel = ALUA_X;
	 regs_ld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'hba) begin
	 // TSX
	 aluop = alu.OPA;
	 alua_sel = ALUA_S;
	 regxld = 1;
	 nld = 1;
	 zld = 1;
	 pcinc = 0;
	 lastcyc = 1;
      end else if (ir == 'hea) begin
	 // NOP
	 pcinc = 0;
	 lastcyc = 1;
      end

      if (adr_mode_done) begin
	 if (instr_type == INSTR_ALU) begin
	    aluop = {1'b0, ir[7:5]};
	    accld = 1;
	    nld = 1;
	    zld = 1;
	    if (ir[7:5] == alu.OPCMP) begin
	       accld = 0;
	       cld = 1;
	       vld = 1;
	    end else if (ir[7:5] == alu.OPADC) begin
	       // TODO: delay with decimal mode
	       cld = 1;
	       vld = 1;
	    end else if (ir[7:5] == alu.OPSBC) begin
	       cld = 1;
	       vld = 1;
	    end
	    lastcyc = 1;
	 end else if (instr_type == INSTR_RMW) begin
	    inc_rmwcyccnt = 1;
	    case (rmwcyccnt)
	      0: begin
		 pcinc = 0;
		 aluop = alu.OPB;
		 dreg_ld = 1;
	      end
	      1: begin
		 pcinc = 0;
		 aluop = {1'b1, ir[7:5]};
		 alua_sel = ALUA_D;
		 dreg_ld = 1;
		 nld = 1;
		 zld = 1;
	      end
	      2: begin
		 we_o = 1;
		 dato_sel = DATO_D;
		 lastcyc = 1;
	      end
	    endcase
	 end else if (instr_type == INSTR_LDXY) begin
	    aluop = alu.OPB;
	    regxld = (ir[1] == 1);
	    regyld = (ir[1] == 0);
	    nld = 1;
	    zld = 1;
	    lastcyc = 1;
	 end else if (instr_type == INSTR_STA) begin
	    dato_sel = DATO_ACC;
	    we_o = 1;
	    lastcyc = 1;
	 end else if (instr_type == INSTR_STXY) begin
	    dato_sel = (ir[1] == 1) ? DATO_X : DATO_Y;
	    we_o = 1;
	    lastcyc = 1;
	 end else if (instr_type == INSTR_CPXY) begin
	    aluop = alu.OPCMP;
	    alua_sel = ir[5] ? ALUA_X : ALUA_Y;
	    nld = 1;
	    zld = 1;
	    cld = 1;
	    vld = 1;
	    lastcyc = 1;
	 end else if (instr_type == INSTR_BIT) begin
	    // N and V load from dat_i on INSTR_BIT
	    nld = 1;
	    vld = 1;
	    zld = 1;
	    aluop = alu.OPAND;
	    lastcyc = 1;
	 end
      end
   end

   always @ (posedge clk_i)
     if (rst_i == 1)
       interrupt <= INT_RES;
     else if (!halt && lastcyc == 1)
       if (nmi_pending)
	 interrupt <= INT_NMI;
       else if (irq && !regf[I_IND])
	 interrupt <= INT_IRQ;
       else
	 interrupt <= INT_NONE;

   always @ (posedge clk_i)
     if (rst_i == 1)
       nmi_pending <= 0;
     // This condition can be added to nmi_pending above to speed up
     // NMI detection.
     else if (nmi && !last_nmi)
       nmi_pending <= 1;
     else if (!halt && lastcyc == 1)
       nmi_pending <= 0;

   always @ (posedge clk_i)
     last_nmi <= nmi;

   always @ (posedge clk_i)
     if (rst_i == 1)
       cyccnt <= 0;
     else if (!halt)
       if (lastcyc == 1)
	 cyccnt <= 0;
       else if (skipcyc == 1)
	 cyccnt <= cyccnt + 2;
       else
	 cyccnt <= cyccnt + 1;

   always @ (posedge clk_i)
     if (rst_i == 1)
       rmwcyccnt <= 0;
     else if (!halt)
       if (lastcyc == 1)
	 rmwcyccnt <= 0;
       else if (inc_rmwcyccnt)
	 rmwcyccnt <= rmwcyccnt + 1;

   assign {pc_c, pc_rel_low} = pc[7:0] + dreg;

   always @ (posedge clk_i)
     if (rst_i)
       pc <= 0;  // Only to avoid undefined address.
     else if (!halt)
       if (pcinc == 1)
	 pc <= pc + 1;
       else begin
	  if (pc_low_ld)
	    pc[7:0] <= dat_i;
	  else if (pc_rel_low_ld)
	    pc[7:0] <= pc_rel_low;
	  else if (pc_dreg_ld)
	    pc[7:0] <= dreg;

	  if (pc_high_ld)
	    pc[15:8] <= dat_i;
	  else if (pc_rel_high_ld)
	    pc[15:8] <= dreg[7] ? pc[15:8] - 1 : pc[15:8] + 1;
       end

   always @ (posedge clk_i)
     if (!halt && irld == 1)
       ir <= dat_i;

   always @ (posedge clk_i)
     if (!halt && dreg_ld == 1)
       dreg <= aluout;

   always @ (posedge clk_i)
     if (!halt && accld == 1)
       acc <= aluout;

   always @ (posedge clk_i)
     if (!halt && regxld == 1)
       regx <= aluout;

   always @ (posedge clk_i)
     if (!halt && regyld == 1)
       regy <= aluout;

   always @ (posedge clk_i)
     if (!halt && regs_ld == 1)
       regs <= aluout;

   always @ (posedge clk_i) begin
      if (!halt)
	if (fld == 1) begin
	   regf[5:4] <= dat_i[7:6];
	   regf[3:0] <= dat_i[3:0];
	end
	else begin
	   if (cld == 1)
	     regf[C_IND] <= alucout;
	   else if (ir == 'h18 && lastcyc == 1)
	     regf[C_IND] <= 0;
	   else if (ir == 'h38 && lastcyc == 1)
	     regf[C_IND] <= 1;

	   if (vld == 1)
	     regf[V_IND] <= (instr_type == INSTR_BIT) ? dat_i[6] : aluvout;
	   else if (ir == 'hb8 && lastcyc == 1)
	     regf[V_IND] <= 0;

	   if (zld == 1)
	     regf[Z_IND] <= (aluout == 0);

	   if (nld == 1)
	     regf[N_IND] <= (instr_type == INSTR_BIT) ? dat_i[7] : aluout[7];

	   if (ir == 'h58 && lastcyc == 1)
	     regf[I_IND] <= 0;
	   else if ((ir == 'h78 && lastcyc == 1) ||
		    (interrupt != INT_NONE && cyccnt == 5))
	     regf[I_IND] <= 1;

	   if (ir == 'hd8 && lastcyc == 1)
	     regf[D_IND] <= 0;
	   else if (ir == 'hf8 && lastcyc == 1)
	     regf[D_IND] <= 1;
	end
   end

   cpu6502_alu alu(.a(alua), .b(alub), .op(aluop),
		   .out(aluout), .cin(regf[C_IND]), .cout(alucout),
		   .vout(aluvout),
		   .d(alud));

   cpu6502_adr adr(.clk(clk_i), .rst(rst_i), .halt(halt), .dat_i(dat_i),
		   .regx(regx), .regy(regy), .regs(regs), .pc(pc),
		   .dreg(dreg),
		   .adro_sel(adro_sel), .indx_sel(adr_indx_sel),
		   .base_sel(adr_base_sel),
		   .zp(adr_zp),
		   .interrupt(interrupt),
		   .low_ld(adr_low_ld), .high_ld(adr_high_ld),
		   .adr_c(adr_c),
		   .adr_out(adr_o));
   
   always @ (dato_sel or acc or pc or regf or regx or regy or dreg) begin
      case (dato_sel)
	DATO_ACC: dat_o = acc;
	DATO_PCL: dat_o = pc[7:0];
	DATO_PCH: dat_o = pc[15:8];
	DATO_FLG: dat_o = {regf[5:4], 1'b1, (ir == 'h00), regf[3:0]};
	DATO_X:   dat_o = regx;
	DATO_Y:   dat_o = regy;
	DATO_D:   dat_o = dreg;
	default:  dat_o = acc;
      endcase
   end

endmodule // cpu6502
