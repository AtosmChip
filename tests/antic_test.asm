;; Atosm Chip
;; Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	.include "atari.inc"

	.code
start:	ldx #$ff
	txs

	lda #0
	ldx #$1f
resetl:	sta $d000,x
	sta $d400,x
	dex
	bpl resetl

	lda #<dlist
	sta dlptr
	lda #>dlist
	sta dlptr+1
	lda #$22
	sta dmactl
	lda #$c0
	sta nmien
	lda #$e0
	sta chbase
	lda #1
	sta gtiactl
	lda #2
	sta chrctl

	lda #5
	sta hscrol
	sta vscrol

	ldx #4
coll:	lda colors,x
	sta colpf0,x
	dex
	bpl coll

wait:	jmp wait
	
nmi:	bit nmist
	sta nmires
	bmi dli
	lda #$18
	sta colbak
	rti
	
dli:	lda #$74
	sta colbak
	rti

irq:	rti

	.data

colors:	.byte $28, $ca, $94, $46, $00

dlist:	.byte $70
	.byte $42
	.word line
	.byte $43
	.word line
	.byte $44
	.word line
	.byte $45
	.word line
	.byte $46
	.word line6
	.byte $47
	.word line6
	.byte $48
	.word line4

	.byte $49
	.word line2
	.byte $49
	.word line2

	.byte $4a
	.word line4
	.byte $4a
	.word line4

	.byte $4b
	.word line2
	.byte $4b
	.word line2
	.byte $4b
	.word line2
	.byte $4b
	.word line2

	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2
	.byte $4c
	.word line2

	.byte $4d
	.word line4
	.byte $4d
	.word line4
	.byte $4d
	.word line4
	.byte $4d
	.word line4

	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4
	.byte $4e
	.word line4

	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2
	.byte $4f
	.word line2

	.byte $40
	.byte $52
	.word linehsc
	.byte $12

	.byte $40
	.byte $62
	.word line
	.byte $62
	.word line
	.byte $42
	.word line
	
	.byte $41
	.word dlist

line:	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99

line6:	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $50,$51,$52,$53,$54,$55,$56,$57,$58,$59
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9

line2:	.byte $55,$33,$0f,$ff,$55,$33,$0f,$ff
	.byte $55,$33,$0f,$ff,$55,$33,$0f,$ff	
	.byte $55,$33,$0f,$ff,$55,$33,$0f,$ff	
	.byte $55,$33,$0f,$ff,$55,$33,$0f,$ff	
	.byte $55,$33,$0f,$ff,$55,$33,$0f,$ff	

line4:	.byte $00,$55,$aa,$ff,$05,$af,$1b,$1b
	.byte $00,$55,$aa,$ff,$05,$af,$1b,$1b
	.byte $00,$55,$aa,$ff,$05,$af,$1b,$1b
	.byte $00,$55,$aa,$ff,$05,$af,$1b,$1b
	.byte $00,$55,$aa,$ff,$05,$af,$1b,$1b
		
linehsc:
	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $10,$11,$12,$13,$14,$15,$16,$17
	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $10,$11,$12,$13,$14,$15,$16,$17,$18,$19
	.byte $90,$91,$92,$93,$94,$95,$96,$97,$98,$99
	.byte $10,$11,$12,$13,$14,$15,$16,$17

	.segment "VECTORS"
	.word nmi
	.word start
	.word irq
