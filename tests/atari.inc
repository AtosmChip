;; Atosm Chip
;; Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

runad	= $2e0

ddevic	= $300
dunit	= $301
dcmnd	= $302
dstats	= $303
dbufa	= $304
dtimlo	= $306
dbyt	= $308
daux1	= $30a
daux2	= $30b

icchid	= $340
icdno	= $341
iccmd	= $342
icstat	= $343
icbufa	= $344
icbufl	= $348
icax1	= $34a
icax2	= $34b

kolm0pf	= $d000
kolm1pf	= $d001
kolm2pf	= $d002
kolm3pf	= $d003
kolp0pf	= $d004
kolp1pf	= $d005
kolp2pf	= $d006
kolp3pf	= $d007
kolm0p	= $d008
kolm1p	= $d009
kolm2p	= $d00a
kolm3p	= $d00b
kolp0p	= $d00c
kolp1p	= $d00d
kolp2p	= $d00e
kolp3p	= $d00f
hposp0	= $d000
hposp1	= $d001
hposp2	= $d002
hposp3	= $d003
hposm0	= $d004
hposm1	= $d005
hposm2	= $d006
hposm3	= $d007
sizep0	= $d008
sizep1	= $d009
sizep2	= $d00a
sizep3	= $d00b
sizem	= $d00c
grafp0	= $d00d
grafp1	= $d00e
grafp2	= $d00f
grafp3	= $d010
grafm	= $d011
trig0	= $d010
trig1	= $d011
trig3	= $d013
colpm0	= $d012
colpm1	= $d013
colpm2	= $d014
pal	= $d014
colpm3	= $d015
colpf0	= $d016
colpf1	= $d017
colpf2	= $d018
colpf3	= $d019
colbak	= $d01a
gtiactl	= $d01b
pmcntl	= $d01d
hitclr	= $d01e
consol	= $d01f

audf1	= $d200
pot0	= $d200
audc1	= $d201
pot1	= $d201
audf2	= $d202
pot2	= $d202
audc2	= $d203
pot3	= $d203
audf3	= $d204
audc3	= $d205
audf4	= $d206
audc4	= $d207
audctl	= $d208
potst	= $d208
stimer	= $d209
kbcode	= $d209
skstres	= $d20a
random	= $d20a
potgo	= $d20b
serout	= $d20d
serin	= $d20d
irqen	= $d20e
irqst	= $d20e
skctl	= $d20f
skstat	= $d20f

porta	= $d300
portb	= $d301
pactl	= $d302
pbctl	= $d303

dmactl	= $d400
chrctl	= $d401
dlptr   = $d402
dlptrl	= $d402
dlptrh	= $d403
hscrol	= $d404
vscrol	= $d405
pmbase	= $d407
chbase	= $d409
wsync	= $d40a
vcount	= $d40b
nmien	= $d40e
nmist	= $d40f
nmires  = $d40f

ciomain = $e456
