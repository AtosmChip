;; Atosm Chip
;; Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

testres = $ee00
testfin = $ee01
testline = $ee02
testseq = $ee04

a_val = $17
x_val = $25
y_val = $3e
s_val = $f1

abs_adr = $0222
abs_val = $44
absx_val = $58
absy_val = $69

zp_adr = $3f
zp_val = $7b
zpx_val = $8e
zpy_val = $90

ind_adr = zp_adr - x_val - 2
indx_val = $a3
indy_val = $b6

.macro  start_test seq
	lda #<seq
	sta testseq
	lda #>seq
	sta testseq + 1
	ldx #$ff
	txs
	jsr setup_test
	ldx #s_val
	txs
	ldx #x_val
.endmacro

.macro  end_test
	jsr save_regs
	tsx
	stx reg_s
.endmacro

.macro  test_ok
	lda #1
	sta testres
.endmacro

.macro  test_fail
	lda #0
	sta testres
.endmacro

.macro	expect_a val
	lda reg_a
	cmp #val
	jsr test_eq
.endmacro

.macro	expect_x val
	lda reg_x
	cmp #val
	jsr test_eq
.endmacro

.macro	expect_y val
	lda reg_y
	cmp #val
	jsr test_eq
.endmacro

.macro	expect_s val
	lda reg_s
	cmp #val
	jsr test_eq
.endmacro

.macro	expect_eq
	lda reg_f
	and #2
	jsr test_ne
.endmacro

.macro	expect_ne
	lda reg_f
	and #2
	jsr test_eq
.endmacro

.macro	expect_pl
	lda reg_f
	and #$80
	jsr test_eq
.endmacro

.macro	expect_mi
	lda reg_f
	and #$80
	jsr test_ne
.endmacro

.macro	expect_cc
	lda reg_f
	and #1
	jsr test_eq
.endmacro

.macro	expect_cs
	lda reg_f
	and #1
	jsr test_ne
.endmacro

	.code

start:
	;; LDA
	start_test 1
	lda #0
	end_test
	expect_a 0
	expect_x x_val
	expect_y y_val
	expect_s s_val
	expect_eq
	expect_pl
	expect_cc

	start_test 2
	lda #$55
	end_test
	expect_a $55
	expect_ne
	expect_pl
	expect_cc

	start_test 3
	lda #$aa
	end_test
	expect_a $aa
	expect_ne
	expect_mi
	expect_cc

	start_test 4
	lda abs_adr
	end_test
	expect_a abs_val

	start_test 5
	lda abs_adr,x
	end_test
	expect_a absx_val

	start_test 6
	lda abs_adr,y
	end_test	
	expect_a absy_val

	start_test 7
	lda zp_adr
	end_test
	expect_a zp_val

	start_test 8
	lda zp_adr,x
	end_test
	expect_a zpx_val

	start_test 9
	lda zp_adr,y
	end_test
	expect_a zpy_val

	start_test 10
	lda (ind_adr,x)
	end_test
	expect_a indx_val

	start_test 11
	lda (ind_adr),y
	end_test
	expect_a indy_val

	;; ADC
	;; TODO: v, decimal mode
	start_test 12
	lda #2
	adc #3
	end_test
	expect_a 5
	expect_x x_val
	expect_y y_val
	expect_s s_val
	expect_ne
	expect_pl
	expect_cc

	start_test 13
	lda #2
	adc #$ff
	end_test
	expect_a 1
	expect_ne
	expect_pl
	expect_cs

	start_test 14
	lda #$40
	adc #$50
	end_test
	expect_a $90
	expect_ne
	expect_mi
	expect_cc

	start_test 15
	sec
	lda #$11
	adc #$22
	end_test
	expect_a $34
	expect_ne
	expect_pl
	expect_cc

	start_test 16
	adc abs_adr
	end_test
	expect_a (a_val + abs_val) & $ff

	start_test 17
	adc abs_adr,x
	end_test
	expect_a (a_val + absx_val) & $ff

	start_test 18
	adc abs_adr,y
	end_test	
	expect_a (a_val + absy_val) & $ff

	start_test 19
	adc zp_adr
	end_test
	expect_a (a_val + zp_val) & $ff

	start_test 20
	adc zp_adr,x
	end_test
	expect_a (a_val + zpx_val) & $ff

	start_test 21
	adc zp_adr,y
	end_test
	expect_a (a_val + zpy_val) & $ff

	start_test 22
	adc (ind_adr,x)
	end_test
	expect_a (a_val + indx_val) & $ff

	start_test 23
	adc (ind_adr),y
	end_test
	expect_a (a_val + indy_val) & $ff

	;; SBC
	;; TODO: v, decimal mode
	start_test 24
	sec
	lda #3
	sbc #2
	end_test
	expect_a 1
	expect_x x_val
	expect_y y_val
	expect_s s_val
	expect_ne
	expect_pl
	expect_cs

	start_test 25
	sec
	lda #2
	sbc #$ff
	end_test
	expect_a 3
	expect_ne
	expect_pl
	expect_cc

	start_test 26
	sec
	lda #$40
	sbc #$90
	end_test
	expect_a $b0
	expect_ne
	expect_mi
	expect_cc

	start_test 27
	clc
	lda #$22
	sbc #$11
	end_test
	expect_a $10
	expect_ne
	expect_pl
	expect_cs

	start_test 28
	sec
	sbc abs_adr
	end_test
	expect_a (a_val - abs_val) & $ff

	start_test 29
	sec
	sbc abs_adr,x
	end_test
	expect_a (a_val - absx_val) & $ff

	start_test 30
	sec
	sbc abs_adr,y
	end_test	
	expect_a (a_val - absy_val) & $ff

	start_test 31
	sec
	sbc zp_adr
	end_test
	expect_a (a_val - zp_val) & $ff

	start_test 32
	sec
	sbc zp_adr,x
	end_test
	expect_a (a_val - zpx_val) & $ff

	start_test 33
	sec
	sbc zp_adr,y
	end_test
	expect_a (a_val - zpy_val) & $ff

	start_test 34
	sec
	sbc (ind_adr,x)
	end_test
	expect_a (a_val - indx_val) & $ff

	start_test 35
	sec
	sbc (ind_adr),y
	end_test
	expect_a (a_val - indy_val) & $ff

	;; CMP
	;; TODO: test V, with negative numbers, with carry initially set
	start_test 36
	cmp #a_val
	end_test
	expect_eq
	expect_pl
	expect_cs
	expect_a a_val
	expect_x x_val
	expect_y y_val
	expect_s s_val

	start_test 37
	cmp #a_val - 1
	end_test
	expect_ne
	expect_pl
	expect_cs

	start_test 38
	cmp #a_val + 1
	end_test
	expect_ne
	expect_mi
	expect_cc

	start_test 39
	lda #abs_val
	cmp abs_adr
	end_test
	expect_eq

	start_test 40
	lda #absx_val
	cmp abs_adr,x
	end_test
	expect_eq

	start_test 41
	lda #absy_val
	cmp abs_adr,y
	end_test	
	expect_eq

	start_test 42
	lda #zp_val
	cmp zp_adr
	end_test
	expect_eq

	start_test 43
	lda #zpx_val
	cmp zp_adr,x
	end_test
	expect_eq

	start_test 44
	lda #zpy_val
	cmp zp_adr,y
	end_test
	expect_eq

	start_test 45
	lda #indx_val
	cmp (ind_adr,x)
	end_test
	expect_eq

	start_test 46
	lda #indy_val
	cmp (ind_adr),y
	end_test
	expect_eq

	;; CPX
	;; TODO: test V, with negative numbers, with carry initially set
	;; rest of addressing modes
	start_test 47
	cpx #x_val
	end_test
	expect_eq
	expect_pl
	expect_cs
	expect_a a_val
	expect_x x_val
	expect_y y_val
	expect_s s_val

	start_test 48
	cpx #x_val - 1
	end_test
	expect_ne
	expect_pl
	expect_cs

	start_test 49
	cpx #x_val + 1
	end_test
	expect_ne
	expect_mi
	expect_cc

	start_test 50
	ldx #abs_val
	cpx abs_adr
	end_test
	expect_eq

	;; CLC/SEC
	start_test 51
	sec
	clc
	end_test
	expect_cc
	expect_a a_val
	expect_x x_val
	expect_y y_val
	expect_s s_val

	start_test 52
	clc
	sec
	end_test
	expect_cs
	expect_a a_val
	expect_x x_val
	expect_y y_val
	expect_s s_val

	;; DEX
	start_test 53
	ldx #10
	dex
	end_test
	expect_x 9
	expect_a a_val
	expect_y y_val
	expect_s s_val
	expect_pl
	expect_ne
	
	start_test 54
	ldx #1
	dex
	end_test
	expect_x 0
	expect_pl
	expect_eq
	
	start_test 55
	ldx #0
	dex
	end_test
	expect_x $ff
	expect_mi
	expect_ne
	
	;; TAX
	start_test 56
	tax
	end_test
	expect_a a_val
	expect_x a_val
	expect_y y_val
	expect_s s_val
	expect_pl
	expect_ne
	expect_cc

	start_test 57
	lda #0
	tax
	end_test
	expect_x 0
	expect_pl
	expect_eq
	expect_cc

	start_test 58
	lda #$ff
	tax
	end_test
	expect_x $ff
	expect_mi
	expect_ne
	expect_cc

	;; TXA
	start_test 59
	txa
	end_test
	expect_a x_val
	expect_x x_val
	expect_y y_val
	expect_s s_val
	expect_pl
	expect_ne
	expect_cc

	start_test 60
	ldx #0
	txa
	end_test
	expect_a 0
	expect_pl
	expect_eq
	expect_cc

	start_test 61
	ldx #$ff
	txa
	end_test
	expect_a $ff
	expect_mi
	expect_ne
	expect_cc

	;; TAY
	start_test 62
	tay
	end_test
	expect_a a_val
	expect_y a_val
	expect_x x_val
	expect_s s_val
	expect_pl
	expect_ne
	expect_cc

	start_test 63
	lda #0
	tay
	end_test
	expect_y 0
	expect_pl
	expect_eq
	expect_cc

	start_test 64
	lda #$ff
	tay
	end_test
	expect_y $ff
	expect_mi
	expect_ne
	expect_cc

	;; TYA
	start_test 65
	tya
	end_test
	expect_a y_val
	expect_y y_val
	expect_x x_val
	expect_s s_val
	expect_pl
	expect_ne
	expect_cc

	start_test 66
	ldy #0
	tya
	end_test
	expect_a 0
	expect_pl
	expect_eq
	expect_cc

	start_test 67
	ldy #$ff
	tya
	end_test
	expect_a $ff
	expect_mi
	expect_ne
	expect_cc

	sta testfin
wait:	jmp wait

setup_test:
	lda #abs_val
	sta abs_adr
	lda #absx_val
	sta abs_adr + x_val
	lda #absy_val
	sta abs_adr + y_val

	lda #zp_val
	sta zp_adr
	lda #zpx_val
	sta zp_adr + x_val
	lda #zpy_val
	sta zp_adr + y_val

	lda #$30
	sta ind_adr + x_val
	lda #2
	sta ind_adr + x_val + 1
	lda #indx_val
	sta $230

	lda #$44
	sta ind_adr
	lda #2
	sta ind_adr + 1
	lda #indy_val
	sta $244 + y_val

	clc
	clv
	cli
	cld
	lda #a_val
	ldy #y_val
	rts

save_regs:
	sta reg_a
	stx reg_x
	sty reg_y
	php
	pla
	sta reg_f
	rts

test_eq:
	beq @ok
	test_fail
	rts
@ok:	test_ok
	rts

test_ne:
	bne @ok
	test_fail
	rts
@ok:	test_ok
	rts

int:	rti

	.data
	.byte $70

	.segment "ZEROPAGE"
reg_a:	.res 1
reg_x:	.res 1
reg_y:	.res 1
reg_s:	.res 1
reg_f:	.res 1
	
	.segment "VECTORS"
	.word int
	.word start
	.word int
