// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "cpu6502.v"
`include "memory.v"


module memory(ack_o,
	      clk_i,
	      adr_i,
	      dat_i,
	      dat_o,
	      rst_i,
	      stb_i,
	      we_i);
   input clk_i;
   input adr_i;
   input dat_i;
   input rst_i;
   input stb_i;
   input we_i;
   output ack_o;
   output dat_o;

   wire       clk_i;
   wire [15:0] adr_i;
   wire [7:0] dat_i;
   wire       rst_i;
   wire       stb_i;
   wire       we_i;

   wire       ack_o;
   wire [7:0] dat_o;

   wire       ramstb;
   wire       romstb;
   wire       ramack_o;
   wire       romack_o;
   wire [7:0] ramdat_o;
   wire [7:0] romdat_o;

   ram u_ram(.clk_i(clk_i),
	     .adr_i(adr_i[9:0]),
	     .dat_i(dat_i),
	     .rst_i(rst_i),
	     .stb_i(ramstb),
	     .we_i(we_i),
	     .ack_o(ramack_o),
	     .dat_o(ramdat_o));

   defparam    u_rom.size = 'h1000;
   defparam    u_rom.adrbits = 12;

   rom u_rom(.clk_i(clk_i),
	     .adr_i(adr_i[11:0]),
	     .rst_i(rst_i),
	     .stb_i(romstb),
	     .ack_o(romack_o),
	     .dat_o(romdat_o));

   assign     ack_o = ramack_o | romack_o;
   assign     dat_o = ramstb ? ramdat_o : romdat_o;

   assign ramstb = stb_i & (adr_i[15:10] == 6'b0);
   assign romstb = stb_i & (adr_i[15:12] == 'hf);

endmodule

module cpu6502_test_reg(clk_i, rst_i,
			adr_i, dat_i,
			stb_i, we_i,
			ack_o,
			test_seq_o,
			test_done_o, test_res_o, test_finish_o);
   input clk_i, rst_i;
   input adr_i, dat_i;
   input stb_i, we_i;

   output ack_o;
   output test_seq_o;
   output test_done_o, test_res_o, test_finish_o;

   wire   clk_i, rst_i;
   wire [2:0] adr_i;
   wire [7:0] dat_i;
   wire       stb_i, we_i;
   wire       ack_o;

   reg [15:0] test_seq_o;
   wire       test_done_o, test_res_o;
   reg 	      test_finish_o;

   assign     ack_o = stb_i;

   always @ (posedge clk_i)
     if (rst_i)
       test_finish_o <= 0;
     else if (stb_i && we_i && adr_i == 1)
       test_finish_o <= 1;

   always @ (posedge clk_i)
     if (stb_i && we_i)
       if (adr_i == 4)
	 test_seq_o[7:0] <= dat_i;
       else if (adr_i == 5)
	 test_seq_o[15:8] <= dat_i;

   assign     test_done_o = (stb_i && we_i && adr_i == 0);
   assign     test_res_o = dat_i[0];
endmodule

module cpu6502_tb();
   reg clk_o;
   reg rst_o;
   wire [15:0] adr;
   wire [7:0]  cpudat_i;
   wire [7:0]  cpudat_o;
   wire        cpu_stb_o;
   wire        memory_stb, test_reg_stb;
   wire        cpu_cyc_o;
   wire        we;
   wire        ack;
   wire        memory_ack, test_reg_ack;

   wire [15:0] test_seq;
   wire        test_done, test_res, test_finish;

   reg 	       ok;

   initial begin
      $readmemh("tests/cpu_test.memh", u_memory.u_rom.memory);
      //$monitor("%h %h a=%h %b %h %h", u_cpu.pc, u_cpu.ir, u_cpu.acc,
	//       u_cpu.regf, adr, u_cpu.dat_o);
      ok = 1;
      clk_o = 1;
      rst_o = 0;
      #5 rst_o = 1;
      #10 rst_o = 0;
   end

   always @ (posedge clk_o) begin
      if (test_finish) begin
	 if (ok)
	   $display("OK");
	 else
	   $display("FAILED");
	 $finish;
      end
      if (test_done)
	if (test_res)
	  $display("Test at %d: PASSED", test_seq);
	else begin
	   $display("Test at %d: FAILED", test_seq);
	   ok <= 0;
	end
   end
   
   always begin
      #5 clk_o = ~clk_o;
   end
   
   assign memory_stb = cpu_stb_o && cpu_cyc_o &&
		       (adr[15:10] == 0 || adr[15:12] == 'hf);
   assign test_reg_stb = cpu_stb_o && cpu_cyc_o && (adr[15:8] == 8'hee);

   assign ack = memory_ack | test_reg_ack;

   memory u_memory(.clk_i(clk_o),
		   .adr_i(adr),
		   .dat_i(cpudat_o),
		   .rst_i(rst_o),
		   .stb_i(memory_stb),
		   .we_i(we),
		   .ack_o(memory_ack),
		   .dat_o(cpudat_i));

   cpu6502_test_reg test_reg(.clk_i(clk_o),
			     .rst_i(rst_o),
			     .adr_i(adr[2:0]),
			     .dat_i(cpudat_o),
			     .stb_i(test_reg_stb),
			     .we_i(we),
			     .ack_o(test_reg_ack),
			     .test_seq_o(test_seq),
			     .test_done_o(test_done),
			     .test_res_o(test_res),
			     .test_finish_o(test_finish));

   cpu6502 u_cpu(.clk_i(clk_o),
		 .adr_o(adr),
		 .dat_i(cpudat_i),
		 .rst_i(rst_o),
		 .stb_o(cpu_stb_o),
		 .cyc_o(cpu_cyc_o),
		 .we_o(we),
		 .ack_i(ack),
		 .dat_o(cpudat_o));

endmodule // cpu6502_tb
