// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "atari.v"

module atosm(rst_i, clk_i,
	     atari_clk_i, atari_clk2_i);
   input rst_i, clk_i, atari_clk_i, atari_clk2_i;

   wire rst_i, clk_i, atari_clk_i, atari_clk2_i;

   wire [15:0] adr;
   reg [7:0]  slavedat_o;
   wire [7:0] masterdat_o;
   wire       stb;
   wire       we;
   wire       ack;
   wire       cyc;

   // Masters outputs.
   wire [15:0] cpuadr_o;
   wire [7:0]  cpudat_o;
   wire        cpustb_o;
   wire        cpuwe_o;
   wire        cpucyc_o;

   // Outputs from address decoder.
   reg 	       ramsel, romsel, atarisel;
   reg 	       dummysel;

   // Slaves STB_I signals.
   wire        ramstb, romstb, ataristb;
   wire        dummystb;

   // Slaves DAT_O signals.
   wire [7:0]  ramdat_o, romdat_o, ataridat_o;

   // Slaves ACK_O signals.
   wire        ramack_o, romack_o, atariack_o;

   // Masters ACK_I signals.
   wire        cpuack_i;

   wire       nmi, irq;

   assign     masterdat_o = cpudat_o;
   assign     adr = cpuadr_o;
   assign     we = cpuwe_o;
   assign     stb = cpustb_o;

   assign     cyc = cpucyc_o;

   // Address decoder.
   always @ (adr) begin
      ramsel = 0;
      romsel = 0;
      atarisel = 0;
      dummysel = 0;
      if (adr[15] == 0)
	ramsel = 1;
      else if (adr[15:8] == 8'h80)
	atarisel = 1;
      else if (adr[15:14] == 2'b11)
	romsel = 1;
      else
	dummysel = 1;
   end

   // Slaves stb_i.
   assign ramstb = ramsel & cyc & stb;
   assign romstb = romsel & cyc & stb;
   assign ataristb = atarisel & cyc & stb;
   assign dummystb = dummysel & cyc & stb;

   assign ack = ramack_o | romack_o | atariack_o | dummystb;

   assign cpuack_i = ack;

   // Slaves dat_o mux.
   always @ (ramsel or ramdat_o or
	     romsel or romdat_o or
	     atarisel or ataridat_o)
     if (ramsel)
       slavedat_o = ramdat_o;
     else if (romsel)
       slavedat_o = romdat_o;
     else if (atarisel)
       slavedat_o = ataridat_o;
     else
       slavedat_o = 'hff;

   assign nmi = 0;
   assign irq = 0;

   defparam u_ram.size = 'h8000;
   defparam u_ram.adrbits = 15;

   ram u_ram(.clk_i(clk_i),
	     .adr_i(adr[14:0]),
	     .dat_i(masterdat_o),
	     .rst_i(rst_i),
	     .stb_i(ramstb),
	     .we_i(we),
	     .ack_o(ramack_o),
	     .dat_o(ramdat_o));

   defparam u_rom.size = 'h4000;
   defparam u_rom.adrbits = 14;

   rom u_rom(.clk_i(clk_i),
	     .adr_i(adr[13:0]),
	     .rst_i(rst_i),
	     .stb_i(romstb),
	     .ack_o(romack_o),
	     .dat_o(romdat_o));

   cpu6502 u_cpu(.clk_i(clk_i),
		 .adr_o(cpuadr_o),
		 .dat_i(slavedat_o),
		 .rst_i(rst_i),
		 .stb_o(cpustb_o),
		 .we_o(cpuwe_o),
		 .ack_i(cpuack_i),
		 .dat_o(cpudat_o),
		 .cyc_o(cpucyc_o),
		 .nmi(nmi),
		 .irq(irq));

   atari u_atari(.clk_i(atari_clk_i),
		 .clk2_i(atari_clk2_i),
		 .rst_i(rst_i),
		 .ext_rst_i(rst_i),
		 .ext_clk_i(clk_i),
		 .ext_adr_i(adr[3:0]),
		 .ext_dat_i(masterdat_o),
		 .ext_dat_o(ataridat_o),
		 .ext_we_i(we),
		 .ext_stb_i(ataristb),
		 .ext_ack_o(atariack_o));
		 
endmodule