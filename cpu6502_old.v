// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

      else if (ir[4:0] == 'h01) begin
	 // ALU (b,x)
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      // TODO: add X to DREG here?
	      adro_sel = adr.ADR0_BASE;
	      adr_zp = 1;
	      adr_base_sel = 1;
	      pcinc = 0;
	   end
	   3: begin
	      adro_sel = adr.ADRO_DREGIND;
	      pcinc = 0;
	      adr_low_ld = 1;
	      alua_sel = ALUA_D;
	      aluop = alu.OPINC;
	      dreg_ld = 1;
	   end
	   4: begin
	      adro_sel = adr.ADRO_DREGIND;
	      pcinc = 0;
	      adr_high_ld = 1;
	   end
	   5: begin
	      adro_sel = adr.ADR_BASE;
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir[4:0] == 'h05) begin
	 // ALU b
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir[4:0] == 'h09) begin
	 // ALU #b
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir[4:0] == 'h0d) begin
	 // ALU a
	 case (cyccnt)
	   1: adr_low_ld  = 1;
	   2: adr_high_ld = 1;
	   3: begin
	      adro_sel = adr.ADRO_BASE;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir[4:0] == 'h11) begin
	 // ALU (b),y
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = adr.ADRO_DREG;
	      pcinc = 0;
	      adr_low_ld = 1;
	      alua_sel = ALUA_D;
	      aluop = alu.OPINC;
	      dreg_ld = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_DREG;
	      pcinc = 0;
	      adr_high_ld = 1;
	      adr_indx_sel = 0;
	   end
	   4: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = 0;
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir[4:0] == 'h15) begin
	 // ALU b,x
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      pcinc = 0;
	   end
	   3: begin
	      adro_sel = ADRO_BASEIND;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      adr_indx_sel = 1;
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir[4:0] == 'h19) begin
	 // ALU a,y
	 case (cyccnt)
	   1: begin
	      adr_low_ld  = 1;
	   end
	   2: begin
	      adr_high_ld = 1;
	      adr_indx_sel = 0;
	      adr_delay_en = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = 0;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir[4:0] == 'h1d) begin
	 // ALU a,x
	 case (cyccnt)
	   1: begin
	      adr_low_ld  = 1;
	   end
	   2: begin
	      adr_high_ld = 1;
	      adr_indx_sel = 1;
	      adr_delay_en = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = 1;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if ((ir == 'ha0 || ir == 'ha2)) begin 
	 // LDX/LDY #imm
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      ldxy_exec = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'ha4 || ir == 'ha6 || ir == 'h84 || ir == 'h86) begin 
	 // LDX/LDY b or ST*
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      ldxy_exec = (ir[7:4] == 'ha);
	      stxy_exec = (ir[7:4] == 'h8);
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir == 'hac || ir == 'hae || ir == 'h8c || ir == 'h8e) begin
	 // LDX/LDY a or ST*
	 case (cyccnt)
	   1: adr_low_ld  = 1;
	   2: adr_high_ld = 1;
	   3: begin
	      adro_sel = adr.ADRO_BASE;
	      ldxy_exec = (ir[7:4] == 'ha);
	      stxy_exec = (ir[7:4] == 'h8);
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir == 'hb4 || ir == 'hb6 || ir == 'h94 || ir == 'h96) begin
	 // LDX/LDY b,y/x or ST*
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      pcinc = 0;
	   end
	   3: begin
	      adro_sel = ADRO_BASEIND;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      adr_indx_sel = (ir == 'hb4);
	      ldxy_exec = (ir[7:4] == 'hb);
	      stxy_exec = (ir[7:4] == 'h9);
	      pcinc = 0;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'hbc || ir == 'hbe) begin
	 // LDX/LDY b,y/x
	 case (cyccnt)
	   1: begin
	      adr_low_ld  = 1;
	   end
	   2: begin
	      adr_high_ld = 1;
	      adr_indx_sel = (ir == 'hbc);
	      adr_delay_en = 1;
	   end
	   3: begin
	      adro_sel = adr.ADRO_BASEIND;
	      adr_indx_sel = (ir == 'hbc);
	      ldxy_exec = 1;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir == 'hc0 || ir == 'he0) begin
	 // CPX/Y #b
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      cpxy_exec = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'hc4 || ir == 'he4) begin
	 // CPX/Y b
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      aluop = alu.OPB;
	      dreg_ld = 1;
	   end
	   2: begin
	      adro_sel = ADRO_BASE;
	      adr_base_sel = 1;
	      adr_zp = 1;
	      lastcyc = 1;
	      pcinc = 0;
	   end
	 endcase
      end else if (ir[4:0] == 'h10) begin
	 // Branch
	 case (cyccnt)
	   1: begin
	      adro_sel = adr.ADRO_PC;
	      if (br_taken)
		dreg_ld = 1;
	      else
		lastcyc = 1;
	   end
	   2: begin
	      pcinc = 0;
	      pc_rel_low_ld = 1;
	      lastcyc = (pc_c & dreg[7]) | (~pc_c & ~dreg[7]);
	   end
	   3: begin
	      pcinc = 0;
	      pc_rel_high_ld = 1;
	      lastcyc = 1;
	   end
	 endcase
      end else if (ir == 'h18 || ir == 'h38 || ir == 'h58 || ir == 'h78 ||
		   ir == 'hb8 || ir == 'hd8 || ir == 'hf8) begin
	 // CLx / SEx
	 // Everything is done in regf process.
	 pcinc = 0;
	 lastcyc = 1;
      end

      // execute ALU
      if ((interrupt == INT_NONE) && (ir[1:0] == 2'b01) &&
	  lastcyc && !adr_delay) begin
	 aluop = {1'b0, ir[7:5]};
	 accld = 1;
	 nld = 1;
	 zld = 1;
	 if (ir[7:5] == 3'b100) begin
	    dato_sel = DATO_ACC;
	    we_o = 1;
	    nld = 0;
	    zld = 0;
	    accld = 0;
	 end else if (ir[7:5] == alu.OPCMP) begin
	    accld = 0;
	    cld = 1;
	    vld = 1;
	 end else if (ir[7:5] == alu.OPADC) begin
	    // TODO: delay with decimal mode
	    cld = 1;
	    vld = 1;
	 end else if (ir[7:5] == alu.OPSBC) begin
	    cld = 1;
	    vld = 1;
	 end
      end

      // execute LDX/LDY
      if ((interrupt == INT_NONE) && ldxy_exec && adr_delay == 0) begin
	 aluop = alu.OPB;
	 regxld = (ir[1] == 1);
	 regyld = (ir[1] == 0);
	 nld = 1;
	 zld = 1;
      end

      // execute STX/STY
      if ((interrupt == INT_NONE) && stxy_exec && adr_delay == 0) begin
	 dato_sel = (ir[1] == 1) ? DATO_X : DATO_Y;
	 we_o = 1;
      end
