// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "cpu6502.v"
`include "memory.v"
`include "antic.v"
`include "gtia.v"
`include "pia.v"
`include "pokey.v"

module delay(clk, in, out);
   input clk, in;
   output out;

   wire   clk, in;
   reg 	  out;
   reg 	  tmp;

   always @ (posedge clk) begin
      tmp <= in;
      out <= tmp;
   end
endmodule

// Clock:
// clk2: 01010101
// clk:  00110011

// TODO: do something with reset, maybe reset Atari only with registers
module atari(clk_i, clk2_i, rst_i,
	     ext_rst_i, ext_clk_i,
	     ext_adr_i,
	     ext_dat_i, ext_dat_o,
	     ext_we_i, ext_stb_i,
	     ext_ack_o,
	     key_code, key_pressed,
	     key_shift, key_break,
	     key_start, key_select, key_option,
	     joystick, trig);
   input clk_i;
   input clk2_i;
   input rst_i;
   input ext_rst_i, ext_clk_i;
   input ext_adr_i;
   input ext_dat_i;
   input ext_we_i, ext_stb_i;
   input key_code, key_pressed;
   input key_shift, key_break;
   input key_start, key_select, key_option;
   input joystick, trig;

   output ext_dat_o, ext_ack_o;

   wire  clk_i;
   wire  clk2_i;
   wire  rst_i;
   wire  ext_rst_i, ext_clk_i;
   wire [3:0] ext_adr_i;
   wire [7:0] ext_dat_i;
   reg [7:0]  ext_dat_o;
   wire       ext_we_i, ext_stb_i, ext_ack_o;
   wire [7:0] key_code;
   wire       key_pressed;
   wire       key_shift, key_break;
   wire       key_start, key_select, key_option;
   wire [7:0] joystick;
   wire [1:0] trig;

   wire  nmi, irq;

   wire [7:0] portb;

   wire [2:0] antic_out;
   wire [7:0] color;
   wire       hsync, vsync;

   wire [7:0] gtia_dmadat_i;
   wire [3:0] consol_out;

   // Serial input/output signals.
   reg [7:0]  serin;
   wire [7:0] serout;
   reg 	      serin_rdy, serout_ack;
   wire       serin_ack, serout_rdy;
   wire       serin_rdy_d, serin_ack_d, serout_rdy_d, serout_ack_d;
   wire       sercmd;

   // Common interconnect signals.
   reg [15:0] adr;
   reg [7:0]  slavedat_o;
   reg [7:0]  masterdat_o;
   reg 	      stb;
   reg 	      we;
   wire       ack;
   wire       cyc;

   // Masters outputs.
   wire [15:0] cpuadr_o, anticadr_o;
   wire [7:0]  cpudat_o;
   wire        cpustb_o, anticstb_o;
   wire        cpuwe_o;
   wire        cpucyc_o, anticcyc_o;

   // Outputs from address decoder.
   reg 	       ramsel, romsel, bassel;
   reg 	       anticsel, gtiasel, pokeysel, piasel;
   reg 	       dummysel;

   // Outputs from arbiter.
   wire	       cpugnt, anticgnt;

   // Slaves STB_I signals.
   wire        ramstb, romstb, basstb;
   wire        anticstb_i, gtiastb, pokeystb, piastb;
   wire        dummystb;

   // Slaves DAT_O signals.
   wire [7:0]  ramdat_o, romdat_o, basdat_o;
   wire [7:0]  anticdat_o, gtiadat_o, pokeydat_o, piadat_o;

   // Slaves ACK_O signals.
   wire        ramack_o, romack_o, basack_o;
   wire        anticack_o, gtiaack_o, pokeyack_o, piaack_o;

   // Masters ACK_I signals.
   wire        cpuack_i, anticack_i;

   // External bus and registers.

   assign      ext_ack_o = ext_stb_i;

   always @ (ext_adr_i)
     if (ext_adr_i == 'h0)
       ext_dat_o = serout;
     else if (ext_adr_i == 'h2)
       ext_dat_o = {sercmd,
		    serout_rdy_d && !serout_ack,
		    !serin_ack_d && !serin_rdy};
     else
       ext_dat_o = 'hff;
   
   // Serial input data and control signals.
   always @ (posedge ext_clk_i) begin
      if (rst_i)
	serin_rdy <= 0;
      else begin
	 if (ext_we_i && ext_stb_i && ext_adr_i == 'h1) begin
	    serin <= ext_dat_i;
	    if (!serin_ack_d)
	      serin_rdy <= 1;
	 end
	 if (serin_ack_d)
	   serin_rdy <= 0;
      end
   end
   
   // Serial output control signals.
   always @ (posedge ext_clk_i)
     if (rst_i)
       serout_ack <= 0;
     else if (!ext_we_i && ext_stb_i && ext_adr_i == 'h0 && serout_rdy_d)
       serout_ack <= 1;
     else if (!serout_rdy_d)
       serout_ack <= 0;

   delay u_serin_rdy_delay(.clk(clk_i), .in(serin_rdy), .out(serin_rdy_d));
   delay u_serin_ack_delay(.clk(ext_clk_i), .in(serin_ack), .out(serin_ack_d));
   delay u_serout_rdy_delay(.clk(ext_clk_i),
			    .in(serout_rdy), .out(serout_rdy_d));
   delay u_serout_ack_delay(.clk(clk_i), .in(serout_ack), .out(serout_ack_d));

   // Arbiter.
   // TODO: should it be synchronous?
   assign      cpugnt = cpucyc_o && !anticcyc_o;
   assign      anticgnt = anticcyc_o;
   assign      cyc = cpucyc_o | anticcyc_o;

   // Masters outputs multiplexer.
   always @ (cpuadr_o or cpudat_o or cpuwe_o or cpustb_o or
	     anticadr_o or anticstb_o or
	     cpugnt or anticgnt) begin
      if (anticgnt) begin
	 adr = anticadr_o;
	 masterdat_o = 0;
	 we = 0;
	 stb = anticstb_o;
      end else begin
	 adr = cpuadr_o;
	 masterdat_o = cpudat_o;
	 we = cpuwe_o;
	 stb = cpustb_o;
      end
   end

   // Address decoder.
   always @ (adr or portb) begin
      ramsel = 0;
      romsel = 0;
      bassel = 0;
      anticsel = 0;
      gtiasel = 0;
      pokeysel = 0;
      piasel = 0;
      anticsel = 0;
      dummysel = 0;
      if (adr[15:7] == {'h50, 1'b1} && !portb[7])
	romsel = 1;
      else if (adr[15:8] >= 'ha0 && adr[15:8] < 'hc0 && !portb[1])
	bassel = 1;
      else if (adr[15:8] == 'hd0)
	gtiasel = 1;
      else if (adr[15:8] == 'hd2)
	pokeysel = 1;
      else if (adr[15:8] == 'hd3)
	piasel = 1;
      else if (adr[15:8] == 'hd4)
	anticsel = 1;
      else if (adr[15:8] == 'hd1 || adr[15:8] == 'hd5 || adr[15:8] == 'hd6 ||
	       adr[15:8] == 'hd7)
	dummysel = 1;
      else if (adr[15:8] >= 'hc0 && portb[0])
	romsel = 1;
      else
	ramsel = 1;
   end

   // Slaves STB_I.
   assign ramstb = ramsel & cyc & stb;
   assign romstb = romsel & cyc & stb;
   assign basstb = bassel & cyc & stb;
   assign anticstb_i = anticsel & cyc & stb;
   assign gtiastb = gtiasel & cyc & stb;
   assign pokeystb = pokeysel & cyc & stb;
   assign piastb = piasel & cyc & stb;
   assign dummystb = dummysel & cyc & stb;

   // Or'd slaves ACK_O.
   assign ack = ramack_o | romack_o | basack_o | anticack_o | gtiaack_o |
		pokeyack_o | piaack_o | dummystb;

   // Slaves DAT_O multiplexer.
   always @ (ramsel or ramdat_o or
	     romsel or romdat_o or
	     bassel or basdat_o or
	     anticsel or anticdat_o or
	     gtiasel or gtiadat_o or
	     pokeysel or pokeydat_o or
	     piasel or piadat_o)
     if (ramsel)
       slavedat_o = ramdat_o;
     else if (romsel)
       slavedat_o = romdat_o;
     else if (bassel)
       slavedat_o = basdat_o;
     else if (anticsel)
       slavedat_o = anticdat_o;
     else if (gtiasel)
       slavedat_o = gtiadat_o;
     else if (pokeysel)
       slavedat_o = pokeydat_o;
     else if (piasel)
       slavedat_o = piadat_o;
     else
       slavedat_o = 'hff;

   // Masters ACK_I signals.
   assign anticack_i = ack & anticgnt;
   assign cpuack_i = ack & cpugnt;

   defparam    u_ram.size = 'h10000;
   defparam    u_ram.adrbits = 16;

   ram u_ram(.clk_i(clk_i),
	     .adr_i(adr),
	     .dat_i(cpudat_o),
	     .rst_i(rst_i),
	     .stb_i(ramstb),
	     .we_i(we),
	     .ack_o(ramack_o),
	     .dat_o(ramdat_o));

   defparam    u_rom.size = 'h4000;
   defparam    u_rom.adrbits = 14;

   rom u_rom(.clk_i(clk_i),
	     .adr_i(adr[13:0]),
	     .rst_i(rst_i),
	     .stb_i(romstb),
	     .ack_o(romack_o),
	     .dat_o(romdat_o));

   defparam    u_basic_rom.size = 'h2000;
   defparam    u_basic_rom.adrbits = 13;

   rom u_basic_rom(.clk_i(clk_i),
		   .adr_i(adr[12:0]),
		   .rst_i(rst_i),
		   .stb_i(basstb),
		   .ack_o(basack_o),
		   .dat_o(basdat_o));

   cpu6502 u_cpu(.clk_i(clk_i),
		 .adr_o(cpuadr_o),
		 .dat_i(slavedat_o),
		 .rst_i(rst_i),
		 .stb_o(cpustb_o),
		 .we_o(cpuwe_o),
		 .ack_i(cpuack_i),
		 .dat_o(cpudat_o),
		 .cyc_o(cpucyc_o),
		 .nmi(nmi),
		 .irq(irq));

   antic u_antic(.clk_i(clk_i),
		 .adr_i(adr[3:0]),
		 .adr_o(anticadr_o),
		 .slavedat_i(cpudat_o),
		 .masterdat_i(slavedat_o),
		 .rst_i(rst_i),
		 .stb_i(anticstb_i),
		 .stb_o(anticstb_o),
		 .we_i(we),
		 .ack_i(anticack_i),
		 .ack_o(anticack_o),
		 .dat_o(anticdat_o),
		 .cyc_o(anticcyc_o),
		 .clk2_i(clk2_i),
		 .nmi(nmi),
		 .antic_out(antic_out));

   assign      gtia_dmadat_i = we ? masterdat_o : slavedat_o;

   gtia u_gtia(.clk_i(clk_i),
	       .adr_i(adr[4:0]),
	       .dat_i(cpudat_o),
	       .rst_i(rst_i),
	       .stb_i(gtiastb),
	       .we_i(we),
	       .ack_o(gtiaack_o),
	       .dat_o(gtiadat_o),
	       .clk2_i(clk2_i),
	       .dmadat_i(gtia_dmadat_i),
	       .antic_out(antic_out),
	       .color(color),
	       .hsync(hsync),
	       .vsync(vsync),
	       // trig[3] = 0 - no cartridge
	       .trig_in({2'b0, ~trig}),
	       .consol_in({1'b0, ~key_start, ~key_select, ~key_option}),
	       .consol_out(consol_out));

   pia u_pia(.clk_i(clk_i),
	     .adr_i(adr[1:0]),
	     .dat_i(cpudat_o),
	     .rst_i(rst_i),
	     .stb_i(piastb),
	     .we_i(we),
	     .ack_o(piaack_o),
	     .dat_o(piadat_o),
	     .pa_i(~joystick),
	     .pb_o(portb),
	     .cb2_o(sercmd));

   pokey u_pokey(.clk_i(clk_i),
		 .adr_i(adr[3:0]),
		 .dat_i(cpudat_o),
		 .rst_i(rst_i),
		 .stb_i(pokeystb),
		 .we_i(we),
		 .ack_o(pokeyack_o),
		 .dat_o(pokeydat_o),
		 .irq(irq),
		 .key_code(key_code), .key_pressed(key_pressed),
		 .key_shift(key_shift), .key_break(key_break),
		 .serout(serout), .serout_rdy_o(serout_rdy),
		 .serout_ack_i(serout_ack_d),
		 .serin(serin), .serin_rdy_i(serin_rdy_d),
		 .serin_ack_o(serin_ack));

endmodule
