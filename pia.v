// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// TODO: interrupt input/outputs

module pia(rst_i, clk_i,
	   adr_i,
	   dat_i,
	   dat_o,
	   we_i,
	   stb_i,
	   ack_o,
	   pa_i,
	   pb_i,
	   pa_o,
	   pb_o,
	   ca2_o, cb2_o);
   input rst_i;
   input clk_i;
   input adr_i;
   input dat_i;
   input we_i;
   input stb_i;
   input pa_i, pb_i;

   output dat_o;
   output ack_o;
   output pa_o, pb_o;
   output ca2_o, cb2_o;

   wire       rst_i, clk_i;
   wire [1:0] adr_i;
   wire [7:0] dat_i;
   wire       we_i;
   wire       stb_i;
       
   wire       ack_o;

   reg [7:0]  dat_o;

   wire [7:0] pa_i, pb_i;
   wire [7:0] pa_o, pb_o;
   wire       ca2_o, cb2_o;

   reg [7:0]  pa_out_reg, pb_out_reg;
   reg [7:0]  pa_dir_reg, pb_dir_reg;
   reg [1:0]  ca1ctl, cb1ctl;
   reg 	      ddir_a, ddir_b;
   reg [2:0]  ca2ctl, cb2ctl;
   
   assign     ack_o = stb_i;

   assign     pa_o = (pa_out_reg & pa_dir_reg) | ~pa_dir_reg;
   assign     pb_o = (pb_out_reg & pb_dir_reg) | ~pb_dir_reg;

   // Only manual CA2/CB2 output modes are supported.
   assign     ca2_o = ca2ctl[0];
   assign     cb2_o = cb2ctl[0];

   // Read registers.
   always @ (adr_i or pa_i or pb_i)
     case (adr_i)
       0: dat_o = ddir_a ?
		  (pa_i & ~pa_dir_reg) | (pa_out_reg & pa_dir_reg) :
		  pa_dir_reg;
       1: dat_o = ddir_b ?
		  (pb_i & ~pb_dir_reg) | (pb_out_reg & pb_dir_reg) :
		  pb_dir_reg;
       2: dat_o =  {2'b00, ca2ctl, ddir_a, ca1ctl};
       3: dat_o =  {2'b00, cb2ctl, ddir_b, cb1ctl};
     endcase

   // Port A output.
   always @ (posedge clk_i)
     if (rst_i)
       pa_out_reg <= 0;
     else if (stb_i && we_i && adr_i == 0 && ddir_a)
       pa_out_reg <= dat_i;

   // Port A direction.
   always @ (posedge clk_i)
     if (rst_i)
       pa_dir_reg <= 0;
     else if (stb_i && we_i && adr_i == 0 && !ddir_a)
       pa_dir_reg <= dat_i;

   // Port B output.
   always @ (posedge clk_i)
     if (rst_i)
       pb_out_reg <= 0;
     else if (stb_i && we_i && adr_i == 1 && ddir_b)
       pb_out_reg <= dat_i;

   // Port B direction.
   always @ (posedge clk_i)
     if (rst_i)
       pb_dir_reg <= 0;
     else if (stb_i && we_i && adr_i == 1 && !ddir_b)
       pb_dir_reg <= dat_i;

   // Port A control.
   always @ (posedge clk_i)
     if (rst_i) begin
	ca2ctl <= 0;
	ddir_a <= 0;
	ca1ctl <= 0;
     end else if (stb_i && we_i && adr_i == 2) begin
	ca2ctl <= dat_i[5:3];
	ddir_a <= dat_i[2];
	ca1ctl <= dat_i[1:0];
     end

   // Port B control.
   always @ (posedge clk_i)
     if (rst_i) begin
	cb2ctl <= 0;
	ddir_b <= 0;
	cb1ctl <= 0;
     end else if (stb_i && we_i && adr_i == 3) begin
	cb2ctl <= dat_i[5:3];
	ddir_b <= dat_i[2];
	cb1ctl <= dat_i[1:0];
     end

endmodule
