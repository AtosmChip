// Atosm Chip
// Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

`include "atosm.v"
`include "clock.v"

module atosm_sim();
   wire atari_clk;
   wire atari_clk2;
   reg 	clk;

   reg rst_o;

   initial begin
      $readmemh("os/atarios.memh", u_atosm.u_atari.u_rom.memory);
      $readmemh("fw/fw.memh", u_atosm.u_rom.memory);

      $monitor("%h %h", u_atosm.u_cpu.pc, u_atosm.u_atari.u_cpu.pc);

      clk = 0;
      rst_o = 0;
      #5 rst_o = 1;
      #40 rst_o = 0;
      #2_000_000_0;
      $finish;
   end

   always begin
      #1 clk = ~clk;
   end

   clkgen u_clkgen(.clk_o(atari_clk), .clk2_o(atari_clk2));
   atosm u_atosm(.rst_i(rst_o), .clk_i(clk),
		 .atari_clk_i(atari_clk), .atari_clk2_i(atari_clk2));

endmodule
