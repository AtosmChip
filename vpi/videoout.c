/* Atosm Chip
   Copyright (C) 2008 Tomasz Malesinski <tmal@mimuw.edu.pl>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include <stdio.h>
#include <math.h>
#include <png.h>
#include <veriuser.h>
#include <vpi_user.h>

#define HEIGHT 312
#define WIDTH (228 * 2)

static png_byte frame[HEIGHT][WIDTH];
static int frame_num, x, y;

static int clip(int n) {
  if (n < 0) return 0;
  if (n > 255) return 255;
  return n;
}

static void YIQToRGB(float y, float i, float q, float *r, float *g, float *b)
{
  *r = y + 0.956 * i + 0.620 * q;
  *g = y - 0.272 * i - 0.647 * q;
  *b = y - 1.108 * i + 1.705 * q;
}

static void atari_color_to_rgb(float saturation, float hue_a,
			       float hue_b, int col,
			       int *rp, int *gp, int *bp) {
  float y, i, q, r, g, b, ang;

  y = (col & 0xf) / 16.0;
  if (col & 0xf0) {
    ang = 2 * M_PI * ((col >> 4) * hue_a + hue_b);
    i = cos(ang) * saturation;
    q = sin(ang) * saturation;
  } else i = q = 0;
  YIQToRGB(y, i, q, &r, &g, &b);
  *rp = clip(r * 256);
  *gp = clip(g * 256);
  *bp = clip(b * 256);
}

static void init_frame(void) {
  frame_num = 0;
  x = y = 0;
}

static void write_frame(void) {
  png_color palette[256];
  int i;
  for (i = 0; i < 256; i++) {
    int r, g, b;
    atari_color_to_rgb(0.4, 1.0 / 16, -0.1, i, &r, &g, &b);
    palette[i].red = r;
    palette[i].green = g;
    palette[i].blue = b;
  }

  char file_name[32];
  snprintf(file_name, sizeof(file_name), "frame%06d.png", frame_num);
  vpi_printf("Writing %s\n", file_name);

  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    return;

  // TODO: those NULLs are error/warning functions. Should they be set to
  // something?
  png_structp png_ptr =
    png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,
			    NULL, NULL);
  if (!png_ptr)
    return;

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    return;
  }

  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    return;
  }

  png_init_io(png_ptr, fp);

  png_set_IHDR(png_ptr, info_ptr, WIDTH, HEIGHT,
	       8, PNG_COLOR_TYPE_PALETTE, PNG_INTERLACE_NONE,
	       PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  png_set_PLTE(png_ptr, info_ptr, palette, 256);
    
  png_bytep row_pointers[HEIGHT];
  for (i = 0; i < HEIGHT; i++)
    row_pointers[i] = frame[i];

  png_set_rows(png_ptr, info_ptr, row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

  png_destroy_write_struct(&png_ptr, &info_ptr);
  fclose(fp);
}


static int videout_finishframe_checktf(char *user_data)
{
  return 0;
}

static int videout_finishframe_calltf(char *user_data)
{
  if (x > 0 || y > 0) {
    write_frame();
    frame_num++;
  }
  x = y = 0;
  return 0;
}

static int videout_finishline_checktf(char *user_data)
{
  return 0;
}

static int videout_finishline_calltf(char *user_data)
{
  x = 0;
  if (y < HEIGHT) y++;
  return 0;
}

static int videout_outpixel_checktf(char *user_data)
{
  // TODO: check the number of arguments
  return 0;
}

static int videout_outpixel_calltf(char *user_data)
{
  if (x < WIDTH && y < HEIGHT) {
    frame[y][x] =  tf_getp(1);
    x++;
  }
  return 0;
}

void videout_register(void) {
  s_vpi_systf_data tf_data;

  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$finishframe";
  tf_data.calltf    = videout_finishframe_calltf;
  tf_data.compiletf = videout_finishframe_checktf;
  tf_data.sizetf    = 0;
  tf_data.user_data = 0;
  vpi_register_systf(&tf_data);

  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$finishline";
  tf_data.calltf    = videout_finishline_calltf;
  tf_data.compiletf = videout_finishline_checktf;
  tf_data.sizetf    = 0;
  tf_data.user_data = 0;
  vpi_register_systf(&tf_data);

  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$outpixel";
  tf_data.calltf    = videout_outpixel_calltf;
  tf_data.compiletf = videout_outpixel_checktf;
  tf_data.sizetf    = 0;
  tf_data.user_data = 0;
  vpi_register_systf(&tf_data);
}

void (*vlog_startup_routines[])() = {
  videout_register,
  init_frame,
  0
};
